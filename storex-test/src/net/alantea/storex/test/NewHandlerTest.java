package net.alantea.storex.test;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.Handler;
import net.alantea.storex.handlers.HandlerException;
import net.alantea.storex.handlers.Names;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NewHandlerTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String STRINGCONTENT = "My string value";

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T1_SimpleHandlerTest() throws StorexException
   {
      Handler.prependHandler(NewHandler1.class);
      HandledObject1 wobject = new HandledObject1();
         wobject.setValue(STRINGCONTENT);
         Writer.write(DATABASEPATH, wobject);

         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof HandledObject1);
         HandledObject1 object1 = (HandledObject1) robject;
         Assert.assertEquals(STRINGCONTENT, object1.getValue());
   }
}

class HandledObject1
{
   private transient String value;
   
   public HandledObject1() {}
   
   public String getValue()
   {
      return value;
   }
   public void setValue(String value)
   {
      this.value = value;
   }
}

class NewHandler1 extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CONTENTTAG, true);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, true);
  }};

   public NewHandler1(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      String content = attributes.get(Names.CONTENTTAG);
      HandledObject1 handled = new HandledObject1();
      handled.setValue(content);
      setTarget(handled, father, field);
   }
   
   public NewHandler1(Object target)
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      write("Handled1", name,Names.CONTENTTAG, ((HandledObject1) target).getValue());
   }
   
   /**
    * On end.
    *
    * @throws HandlerException the handler exception
    */
   @Override
   public void onEnd(String characters) throws HandlerException
   {
      // default : empty
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return (HandledObject1.class.equals(target.getClass()));
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return "Handled1".equals(type);
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}