package net.alantea.storex.test;

import static org.junit.Assert.fail;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ImageTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T0_ImageCreationTest()
   {
      try
      {
         BufferedImage object = ImageIO.read(new File("sample.png"));
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException | IOException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }

   @Test
   public void T1_ImageReadingTest() throws IOException
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assert.assertNotNull(object);
         Assert.assertTrue(object instanceof BufferedImage);
         BufferedImage reference = ImageIO.read(new File("sample.png"));
         Assert.assertTrue(compareImage(reference, (BufferedImage) object));
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   public static boolean compareImage(BufferedImage reference, BufferedImage object) {        
      try {
         DataBuffer dbA = reference.getData().getDataBuffer();
         int sizeA = dbA.getSize();                   
         DataBuffer dbB = object.getData().getDataBuffer();
         int sizeB = dbB.getSize();
         // compare data-buffer objects //
         if(sizeA == sizeB) {
             for(int i=0; i<sizeA; i++) { 
                 if(dbA.getElem(i) != dbB.getElem(i)) {
                     return false;
                 }
             }
             return true;
         }
         else {
             return false;
         }
     } 
     catch (Exception e) { 
         System.out.println("Failed to compare image files ...");
         return  false;
     }
 }
}
