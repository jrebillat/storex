package net.alantea.storex.test;

import java.awt.Point;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.ListProperty;
import net.alantea.liteprops.LongProperty;
import net.alantea.liteprops.MapProperty;
import net.alantea.liteprops.Property;
import net.alantea.liteprops.StringProperty;
import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PropertiesTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String STRINGCONTENT = "TestString";
   private static final int INTCONTENT = 123;
   private static final long LONGCONTENT = 123456789L;
   private static final boolean BOOLEANCONTENT = true;

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T1_SimplePropertyCreationTest() throws StorexException
   {
         PropertyObject1 wobject = new PropertyObject1();
         wobject.setValue(STRINGCONTENT);
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject1);
         PropertyObject1 object1 = (PropertyObject1) robject;
         Assert.assertEquals(STRINGCONTENT, object1.getValue());
   }
   
   @Test
   public void T2_ObjectWithPropertyFieldsCreationTest() throws StorexException
   {
         PropertyObject2 wobject = new PropertyObject2();
         wobject.setValue(STRINGCONTENT);
         wobject.setIntValue(INTCONTENT);
         wobject.setLongValue(LONGCONTENT);
         wobject.setBooleanValue(BOOLEANCONTENT);
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject2);
         PropertyObject2 object2 = (PropertyObject2) robject;
         Assert.assertEquals(STRINGCONTENT, object2.getValue());
         Assert.assertEquals(INTCONTENT, object2.getIntValue());
         Assert.assertEquals(LONGCONTENT, object2.getLongValue());
         Assert.assertEquals(BOOLEANCONTENT, object2.getBooleanValue());
   }
   
   @Test
   public void T3_SubObjectWithPropertyCreationTest() throws StorexException
   {
         PropertyObject1 wobject1 = new PropertyObject1();
         wobject1.setValue(STRINGCONTENT);
         PropertyObject3 wobject3 = new PropertyObject3();
         wobject3.setSubObject(wobject1);
         Object wsubObject = wobject3.getSubObject();
         Assert.assertNotNull(wsubObject);
         Writer.write(DATABASEPATH, wobject3);

         Object object = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(object);
         Assert.assertTrue(object instanceof PropertyObject3);
         PropertyObject3 robject3 = (PropertyObject3) object;
         Object subObject = robject3.getSubObject();
         Assert.assertNotNull(subObject);
         Assert.assertTrue(subObject instanceof PropertyObject1);
         PropertyObject1 object1 = (PropertyObject1) subObject;
         Assert.assertEquals(STRINGCONTENT, object1.getValue());
   }
   
   @Test
   public void T4_ListPropertyTest() throws StorexException
   {
         PropertyObject4 wobject = new PropertyObject4();
         wobject.add(STRINGCONTENT);
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject4);
         PropertyObject4 object4 = (PropertyObject4) robject;
         List<String> list = object4.getList();
         Assert.assertNotNull(list);
         Assert.assertEquals(1, list.size());
         Assert.assertEquals(STRINGCONTENT, list.get(0));
   }
   
   @Test
   public void T5_ListPropertyPrimitiveTest() throws StorexException
   {
      PropertyObject5 wobject = new PropertyObject5();
         wobject.getList().add(123);
         wobject.getList().add(456);
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject5);
         PropertyObject5 object1 = (PropertyObject5) robject;
         List<Integer> list = object1.getList();
         Assert.assertTrue(123 == list.get(0));
         Assert.assertTrue(456 == list.get(1));
   }
   
   @Test
   public void T6_ListPropertyObjectTest() throws StorexException
   {
      PropertyObject6 wobject = new PropertyObject6();
         wobject.getList().add(new Point(1, 2));
         wobject.getList().add(new Point(3, 4));
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject6);
         PropertyObject6 object1 = (PropertyObject6) robject;
         List<Point> list = object1.getList();
         Assert.assertEquals(2, list.size());
         Assert.assertEquals(new Point(1, 2), list.get(0));
         Assert.assertEquals(new Point(3, 4), list.get(1));
   }
   
   @Test
   public void T7_ListOfPropertyTest() throws StorexException
   {
      PropertyObject7 wobject = new PropertyObject7();
         wobject.getList().add(new IntegerProperty(147));
         wobject.getList().add(new IntegerProperty(258));
         wobject.getList().add(new IntegerProperty(369));
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject7);
         PropertyObject7 object1 = (PropertyObject7) robject;
         List<IntegerProperty> list = object1.getList();
         Assert.assertTrue(147 == list.get(0).get());
         Assert.assertTrue(258 == list.get(1).get());
         Assert.assertTrue(369 == list.get(2).get());
   }
   
   @Test
   public void T8_MapPropertyObjectTest() throws StorexException
   {
      PropertyObject8 wobject = new PropertyObject8();
         wobject.getMap().put("One", new Point(1, 2));
         wobject.getMap().put("Two", new Point(3, 4));
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof PropertyObject8);
         PropertyObject8 object1 = (PropertyObject8) robject;
         Map<String, Point> map = object1.getMap();
         Assert.assertEquals(2, map.size());
         Set<String> keys = map.keySet();
         Assert.assertTrue(keys.contains("One"));
         Assert.assertEquals(new Point(1, 2), map.get("One"));
         Assert.assertTrue(keys.contains("Two"));
         Assert.assertEquals(new Point(3, 4), map.get("Two"));
   }
}

class PropertyObject1
{
   private StringProperty value = new StringProperty();
   
   public PropertyObject1() {}
   
   public String getValue()
   {
      return value.get();
   }
   public void setValue(String value)
   {
      this.value.set(value);
   }
}

class PropertyObject2
{
   private StringProperty value = new StringProperty();
   private IntegerProperty intValue = new IntegerProperty();
   private LongProperty longValue = new LongProperty();
   private BooleanProperty booleanValue = new BooleanProperty();
   
   public PropertyObject2() {}
   
   public String getValue()
   {
      return value.get();
   }
   public void setValue(String value)
   {
      this.value.set(value);
   }

   public int getIntValue()
   {
      return intValue.get();
   }

   public void setIntValue(int intValue)
   {
      this.intValue.set(intValue);
   }

   public long getLongValue()
   {
      return longValue.get();
   }

   public void setLongValue(long longValue)
   {
      this.longValue.set(longValue);
   }

   public boolean getBooleanValue()
   {
      return booleanValue.get();
   }

   public void setBooleanValue(boolean booleanValue)
   {
      this.booleanValue.set(booleanValue);
   }
}

class PropertyObject3
{
   private Property<PropertyObject1> subObject = new Property<>();
   
   public PropertyObject3() {}

   public PropertyObject1 getSubObject()
   {
      return subObject.get();
   }
   
   public void setSubObject(PropertyObject1 subObject)
   {
      this.subObject.set(subObject);
   }
}

class PropertyObject4
{
   private ListProperty<String> list = new ListProperty<>();
   
   public PropertyObject4() {}

   public List<String> getList()
   {
      return list.get();
   }
   
   public void add(String value)
   {
      list.add(value);
   }
}

class PropertyObject5
{
   private ListProperty<Integer> list = new ListProperty<>();
   
   public PropertyObject5() {}

   public List<Integer> getList()
   {
      return list.get();
   }
   
   public void add(int value)
   {
      list.add(value);
   }
}

class PropertyObject6
{
   private ListProperty<Point> list = new ListProperty<>();
   
   public PropertyObject6() {}

   public List<Point> getList()
   {
      return list.get();
   }
   
   public void add(Point value)
   {
      list.add(value);
   }
}

class PropertyObject7
{
   private List<IntegerProperty> list = new LinkedList<>();
   
   public PropertyObject7() {}

   public List<IntegerProperty> getList()
   {
      return list;
   }
   
   public void add(IntegerProperty value)
   {
      list.add(value);
   }
}

class PropertyObject8
{
   private MapProperty<String, Point> map = new MapProperty<>();
   
   public PropertyObject8() {}

   public Map<String, Point> getMap()
   {
      return map.get();
   }
   
   public void put(String key, Point value)
   {
      map.put(key, value);
   }
}
