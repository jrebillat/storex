package net.alantea.storex.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MapTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String KEY1 = "Key1";
   private static final String KEY2 = "Key2";
   private static final String KEY3 = "Key3";
   private static final String VALUE1 = "Value1";
   private static final String VALUE2 = "Value2";
   private static final String VALUE3 = "Value3";

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T1_SimpleMapCreationTest()
   {
      try
      {
         MapObject1 wobject = new MapObject1();
         wobject.addValue(KEY1, VALUE1);
         wobject.addValue(KEY2, VALUE2);
         wobject.addValue(KEY3, VALUE3);
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof MapObject1);
         Map<String, String> map = ((MapObject1) robject).getMap();
         Assert.assertNotNull(map);
         assertEquals(VALUE1, map.get(KEY1));
         assertEquals(VALUE2, map.get(KEY2));
         assertEquals(VALUE3, map.get(KEY3));
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   @Test
   public void T2_ObjectsMapCreationTest()
   {
      try
      {
         MapObject2 wobject = new MapObject2();
         wobject.addValue(KEY1, VALUE1);
         wobject.addValue(KEY2, VALUE2);
         wobject.addValue(KEY3, VALUE3);
         Writer.write(DATABASEPATH, wobject);
         
         Object robject = Reader.load(DATABASEPATH);
         Assert.assertNotNull(robject);
         Assert.assertTrue(robject instanceof MapObject2);
         Map<MapObject2Key, MapObject2Value> map = ((MapObject2) robject).getMap();
         Assert.assertNotNull(map);
         Assert.assertNotEquals(0, map.size());
         for (MapObject2Key key : map.keySet())
         {
            MapObject2Value value = map.get(key);
            switch(key.getContent())
            {
               case KEY1 :
                  
                  assertEquals(VALUE1, value.getContent());
                  break;

               case KEY2 :
                  assertEquals(VALUE2, value.getContent());
                  break;

               case KEY3 :
                  assertEquals(VALUE3, value.getContent());
                  break;
                 
            }
         }
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
}

class MapObject1
{
   private Map<String, String> map = new HashMap<>();
   
   public MapObject1() {}
   
   public Map<String, String> getMap()
   {
      return map;
   }
   public void addValue(String key, String value)
   {
      map.put(key, value);
   }
}

class MapObject2Key
{
   private String content;
   public MapObject2Key() {}
   
   public String getContent()
   {
      return content;
   }
   
   public void setContent(String content)
   {
      this.content = content;
   }
}

class MapObject2Value
{
   private String content;
   public MapObject2Value() {}
   
   public String getContent()
   {
      return content;
   }
   
   public void setContent(String content)
   {
      this.content = content;
   }
}

class MapObject2
{
   private Map<MapObject2Key, MapObject2Value> map = new HashMap<>();
   
   public MapObject2() {}
   
   public Map<MapObject2Key, MapObject2Value> getMap()
   {
      return map;
   }
   public void addValue(String key, String value)
   {
      MapObject2Key keyObject = new MapObject2Key();
      keyObject.setContent(key);
      MapObject2Value valueObject = new MapObject2Value();
      valueObject.setContent(value);
      map.put(keyObject, valueObject);
   }
}
