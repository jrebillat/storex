package net.alantea.storex.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
   SimpleContentTest.class,
   SimpleObjectTest.class,
   ConstructorsTest.class,
   ListTest.class,
   ArrayTest.class,
   MapTest.class,
   ComposedObjectsTest.class,
   PropertiesTest.class,
   SpecialObjectsTest.class,
   NewHandlerTest.class,
   ByteArrayTest.class
   })
public class AllTests
{
}
