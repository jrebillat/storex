package net.alantea.storex.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DateTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   
   private static final Date date = new Date(13456789);

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T0_DateCreationTest()
   {
      try
      {
         Writer.write(DATABASEPATH, date);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }

   @Test
   public void T1_DateReadingTest() throws IOException
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assert.assertNotNull(object);
         Assert.assertTrue(object instanceof Date);
         Date newDate = (Date)object;
         Assert.assertEquals(date, newDate);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
   
   @Test
   public void T2_DateObjectCreationTest()
   {
      try
      {
         clear();
         MyDateObject object = new MyDateObject();
         object.setDate(date);
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }

   @Test
   public void T3_DateObjectReadingTest() throws IOException
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assert.assertNotNull(object);
         Assert.assertTrue(object instanceof MyDateObject);
         MyDateObject newDateObject = (MyDateObject)object;
         Date newDate = newDateObject.getDate();
         Assert.assertNotNull(newDate);
         Assert.assertEquals(date, newDate);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         fail(e.getMessage());
      }
   }
}

class MyDateObject
{
   Date date;

   public MyDateObject() {}

   public Date getDate()
   {
      return date;
   }

   public void setDate(Date date)
   {
      this.date = date;
   }
}
