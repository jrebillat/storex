package net.alantea.storex.test;

import java.io.File;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.test.SpecialObject2.Value;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SpecialObjectsTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T1_SimpleObjectCreationTest() throws StorexException
   {
      SpecialObject1 object1 = new SpecialObject1();
      Date savedDate = object1.getDate();
         Writer.write(DATABASEPATH, object1);
         
         Object object2 = (Object) Reader.load(DATABASEPATH);
         Assert.assertNotNull(object2);
         Assert.assertTrue(object2 instanceof SpecialObject1);
         SpecialObject1 sobject2 = (SpecialObject1)object2;
         Assert.assertNotNull(sobject2.getDate());
         Assert.assertEquals(savedDate.getTime(), sobject2.getDate().getTime());
   }
   
   @Test
   public void T2_EnumTest() throws StorexException
   {
      SpecialObject2 object1 = new SpecialObject2();
      Assert.assertEquals(Value.VAL1, object1.getValue());
      object1.setValue(Value.VAL2);
      Assert.assertEquals(Value.VAL2, object1.getValue());
      Writer.write(DATABASEPATH, object1);
         
      Object object2 = (Object) Reader.load(DATABASEPATH);
      Assert.assertNotNull(object2);
      Assert.assertTrue(object2 instanceof SpecialObject2);
      SpecialObject2 sobject2 = (SpecialObject2)object2;
      Assert.assertEquals(Value.VAL2, sobject2.getValue());
   }
   
   @Test
   public void T3_ArrayTest() throws StorexException
   {
      SpecialObject3 object1 = new SpecialObject3();
      String[] vals = { "Me", "You", "Everybody" };
      object1.setValues(vals);
      Writer.write(DATABASEPATH, object1);
         
      Object object2 = (Object) Reader.load(DATABASEPATH);
      Assert.assertNotNull(object2);
      Assert.assertTrue(object2 instanceof SpecialObject3);
      SpecialObject3 sobject2 = (SpecialObject3)object2;
      String[] found = sobject2.getValues();
      Assert.assertEquals(3, found.length);
      Assert.assertEquals("Me", found[0]);
      Assert.assertEquals("You", found[1]);
      Assert.assertEquals("Everybody", found[2]);
   }
}

class SpecialObject1
{
   @SuppressWarnings("unused")
   private static String value1;
   @SuppressWarnings("unused")
   private transient String value2;
   @SuppressWarnings("unused")
   private final String value3;
   
   private Date date;
   
   public SpecialObject1() {
      value3 = "";
   }
   
   public Date getDate()
   {
      if (date == null)
      {
         date =new Date();
      }
      return date;
   }
}

class SpecialObject2
{
   enum Value
   {
      VAL1,
      VAL2,
      VAL3
   }
   
   private Value value = Value.VAL1;
   
   public SpecialObject2() {
   }
   
   public void setValue(Value value)
   {
      this.value = value;
   }

   public Value getValue()
   {
      return value;
   }
}

class SpecialObject3
{
   private String[] values = { "One", "Two" };
      
   public SpecialObject3() {
   }
   
   public String[] getValues()
   {
      return values;
   }
   
   public void setValues(String[] vals)
   {
      values = vals;
   }
}
