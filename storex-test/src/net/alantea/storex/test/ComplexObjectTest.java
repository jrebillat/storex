package net.alantea.storex.test;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ComplexObjectTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String KEY1 = "TestKey1";
   private static final String KEY2 = "TestKey2";
   private static final String CONTENT = "TestContent";

   @BeforeClass
   public static void clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @AfterClass
   public static void close()
   {
//      clear();
   }
   
   @Test
   public void T1_ComplexObjectWithStringTest() throws StorexException
   {
      ComplexObject1 object1 = new ComplexObject1();
      ComplexObject2 object2 = new ComplexObject2();
      ComplexObject3 object3 = new ComplexObject3();
      ComplexObject4 object4 = new ComplexObject4();
      
      object1.addComplexObject2(object2);
      object2.addComplexObject3(object3);
      object3.setKey(KEY1);
      object3.setValue(object4);
      object4.setKey(KEY2);
      object4.setValue(CONTENT);
      
      Writer.write(DATABASEPATH, object1);

      Object root = (Object) Reader.load(DATABASEPATH);
      Assert.assertNotNull(root);
      Assert.assertTrue(root instanceof ComplexObject1);
      ComplexObject1 robject1 = (ComplexObject1) root;
      
      List<ComplexObject2> list2 = robject1.getEntities();
      Assert.assertNotNull(list2);
      Assert.assertEquals(1, list2.size());
      ComplexObject2 robject2 = list2.get(0);
      Assert.assertNotNull(robject2);

      List<ComplexObject3> list3 = robject2.getAttributes();
      Assert.assertNotNull(list3);
      Assert.assertEquals(1, list3.size());
      ComplexObject3 robject3 = list3.get(0);
      Assert.assertNotNull(robject3);
      Assert.assertEquals(KEY1, robject3.getKey());

      ComplexObject4 robject4 = robject3.getValue();
      Assert.assertNotNull(robject4);
      Assert.assertEquals(KEY2, robject4.getKey());
      Assert.assertEquals(CONTENT, robject4.getValue());
   }
}

class ComplexObject1
{
   @SuppressWarnings("unused")
   private List<String> keyAttributes = new LinkedList<>();
   private List<ComplexObject2> entities = new LinkedList<>();
   @SuppressWarnings("unused")
   private List<String> labels = new LinkedList<>();
   
   public ComplexObject1() {}
   
   public List<ComplexObject2> getEntities()
   {
      return entities;
   }
   public void addComplexObject2(ComplexObject2 value)
   {
      entities.add(value);
   }
}

class ComplexObject2
{
   @SuppressWarnings("unused")
   private List<String> labels = new LinkedList<>();
   private List<ComplexObject3> attributes = new LinkedList<>();
   
   public ComplexObject2() {}

   
   public List<ComplexObject3> getAttributes()
   {
      return attributes;
   }
   public void addComplexObject3(ComplexObject3 value)
   {
      attributes.add(value);
   }
}

class ComplexObject3
{
   private String key;
   private ComplexObject4 value;
   
   public ComplexObject3() {}
   
   public String getKey()
   {
      return key;
   }
   public void setKey(String key)
   {
      this.key = key;
   }
   
   public ComplexObject4 getValue()
   {
      return value;
   }
   public void setValue(ComplexObject4 value)
   {
      this.value = value;
   }
}

class ComplexObject4
{
   private String key;
   private String value;
   
   public ComplexObject4() {}
   
   public String getKey()
   {
      return key;
   }
   public void setKey(String key)
   {
      this.key = key;
   }
   
   public String getValue()
   {
      return value;
   }
   public void setValue(String value)
   {
      this.value = value;
   }
}
