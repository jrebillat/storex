package net.alantea.storex.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;


import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class ComposedObjectsTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String STRINGCONTENT1 = "TestString1";
   private static final String STRINGCONTENT2 = "TestString2";
   private static final String STRINGCONTENT3 = "TestString3";
   private static final int INTCONTENT1 = 123;
   private static final int INTCONTENT2 = 456;
   private static final int INTCONTENT3 = 789;
   private static final boolean BOOLEANCONTENT1 = true;
   private static final boolean BOOLEANCONTENT2 = false;

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T1_SimpleObjectCreationTest() throws StorexException
   {
      ComposedObject1 object = new ComposedObject1();
         object.addValue(STRINGCONTENT1);
         object.addValue(STRINGCONTENT2);
         object.addValue(STRINGCONTENT3);
         Writer.write(DATABASEPATH, object);
   }

   @Test
   public void T2_SimpleObjectReadingTest() throws StorexException
   {
         Object object = (Object) Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ComposedObject1);
         ComposedObject1 object1 = (ComposedObject1) object;
         Object val = object1.getValues();
         Assertions.assertNotNull(val);
         Assertions.assertTrue(val instanceof List);
         @SuppressWarnings("unchecked")
         List<String> list = (List<String>) val;
         Assertions.assertEquals(3, list.size());
         Assertions.assertEquals(STRINGCONTENT1, list.get(0));
         Assertions.assertEquals(STRINGCONTENT2, list.get(1));
         Assertions.assertEquals(STRINGCONTENT3, list.get(2));
   }
   
   @Test
   public void T3_DoubleObjectCreationTest() throws StorexException
   {
      ComposedObject2 object = new ComposedObject2();
         object.addIntValue(INTCONTENT1);
         object.addIntValue(INTCONTENT2);
         object.addIntValue(INTCONTENT3);
         object.addBooleanValue(BOOLEANCONTENT1);
         object.addBooleanValue(BOOLEANCONTENT2);
         Writer.write(DATABASEPATH, object);
   }

   @Test
   public void T4_DoubleObjectReadingTest() throws StorexException
   {
         Object object = (Object) Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ComposedObject2);
         ComposedObject2 object2 = (ComposedObject2) object;
         Object val1 = object2.getIntValues();
         Assertions.assertNotNull(val1);
         Assertions.assertTrue(val1 instanceof List);
         @SuppressWarnings("unchecked")
         List<Integer> list1 = (List<Integer>) val1;
         Assertions.assertEquals(3, list1.size());
         Object val2 = object2.getBooleanValues();
         Assertions.assertNotNull(val2);
         Assertions.assertTrue(val2 instanceof List);
         @SuppressWarnings("unchecked")
         List<Boolean> list2 = (List<Boolean>) val2;
         Assertions.assertEquals(2, list2.size());
         Assertions.assertTrue(BOOLEANCONTENT1 == list2.get(0));
   }
   
   @Test
   public void T5_ListOfObjectsCreationTest() throws StorexException
   {
      ComposedObject3 object = new ComposedObject3();
      ComposedObject1 object1 = new ComposedObject1();
         object1.addValue(STRINGCONTENT1);
         object1.addValue(STRINGCONTENT2);
         object.addValue(object1);
         ComposedObject1 object2 = new ComposedObject1();
         object2.addValue(STRINGCONTENT2);
         object2.addValue(STRINGCONTENT3);
         object.addValue(object2);
         Writer.write(DATABASEPATH, object);
   }

   @Test
   public void T6_ListOfObjectsReadingTest() throws StorexException
   {
         Object object = (Object) Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ComposedObject3);
         ComposedObject3 object3 = (ComposedObject3) object;
         Object val = object3.getValues();
         Assertions.assertNotNull(val);
         Assertions.assertTrue(val instanceof List);
         @SuppressWarnings("unchecked")
         List<ComposedObject1> list = (List<ComposedObject1>) val;
         Assertions.assertEquals(2, list.size());
         
         ComposedObject1 object1 = list.get(0);
         Object val1 = object1.getValues();
         Assertions.assertNotNull(val1);
         Assertions.assertTrue(val1 instanceof List);
         @SuppressWarnings("unchecked")
         List<String> list1 = (List<String>) val1;
         Assertions.assertEquals(2, list1.size());
         Assertions.assertEquals(STRINGCONTENT1, list1.get(0));
         Assertions.assertEquals(STRINGCONTENT2, list1.get(1));
         
         ComposedObject1 object2 = list.get(1);
         Object val2 = object2.getValues();
         Assertions.assertNotNull(val2);
         Assertions.assertTrue(val2 instanceof List);
         @SuppressWarnings("unchecked")
         List<String> list2 = (List<String>) val2;
         Assertions.assertEquals(2, list2.size());
         Assertions.assertEquals(STRINGCONTENT2, list2.get(0));
         Assertions.assertEquals(STRINGCONTENT3, list2.get(1));
   }
}

class ComposedObject1
{
   private List<String> values = new ArrayList<>();
   
   public ComposedObject1() {}
   
   public List<String> getValues()
   {
      return values;
   }
   public void addValue(String value)
   {
      this.values.add(value);
   }
}

class ComposedObject2
{
   private List<Integer> intValues = new ArrayList<>();
   private List<Boolean> booleanValues = new ArrayList<>();
   
   public ComposedObject2() {}
   
   public List<Integer> getIntValues()
   {
      return intValues;
   }
   public void addIntValue(int value)
   {
      this.intValues.add(value);
   }
   
   public List<Boolean> getBooleanValues()
   {
      return booleanValues;
   }
   public void addBooleanValue(boolean value)
   {
      this.booleanValues.add(value);
   }
}

class ComposedObject3
{
   private List<ComposedObject1> values = new ArrayList<>();
   
   public ComposedObject3() {}
   
   public List<ComposedObject1> getValues()
   {
      return values;
   }
   public void addValue(ComposedObject1 value)
   {
      this.values.add(value);
   }
}