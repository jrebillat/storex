package net.alantea.storex.test;

import java.io.File;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class SimpleContentTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String OBJECTCONTENT = "TestString";
   private static final String LINECONTENT = "This is a line to insert in the object\n";

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }

   @Test
   public void T1_SimpleStringCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, OBJECTCONTENT);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof String);
      Assertions.assertEquals(OBJECTCONTENT, object);
   }

   @Test
   public void T2_BigStringCreationTest() throws StorexException
   {
      StringBuffer buffer = new StringBuffer();
      for (int i = 0; i < 100; i++)
      {
         buffer.append("line ");
         buffer.append(i);
         buffer.append(" : ");
         buffer.append(LINECONTENT);
      }
      Writer.write(DATABASEPATH, buffer.toString());

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof String);
      Assertions.assertEquals(buffer.toString(), object);
   }

   @Test
   public void T3_SimpleIntegerCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, 666);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Integer);
      Assertions.assertEquals(666, object);
   }

   @Test
   public void T4_SimpleFloatCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, (float) Math.PI);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Float);
      Assertions.assertEquals((float) Math.PI, object);
   }

   @Test
   public void T5_SimpleLongCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, (long) 1234567890);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Long);
      Assertions.assertEquals((long) 1234567890, object);
   }

   @Test
   public void T6_SimpleDoubleCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, Math.PI);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Double);
      Assertions.assertEquals(Math.PI, object);
   }

   @Test
   public void T7_SimpleShortCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, (short) 1234);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Short);
      Assertions.assertEquals((short) 1234, object);
   }

   @Test
   public void T8_SimpleByteCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, (byte) 32);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Byte);
      Assertions.assertEquals((byte) 32, object);
   }

   @Test
   public void T9_SimpleBooleanCreationTest() throws StorexException
   {
      Writer.write(DATABASEPATH, true);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof Boolean);
      Assertions.assertEquals(true, object);
   }
}
