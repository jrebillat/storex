package net.alantea.storex.test;

import java.io.File;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class ArrayTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String VALUE1 = "Value1";
   private static final String VALUE2 = "Value2";
   private static final String VALUE3 = "Value3";
   private static String[] ARRAY1 = { VALUE1, VALUE2, VALUE3};
   private static int[] INTARRAY1 = { 4, 5, 6, 7};
   private static float[] FLOATARRAY1 = { 3.14f, 1.234f};

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T1_SimpleIntegerArrayTest()
   {
      try
      {
         int[] ints = { 1, 2, 3, 4, 5 };
         Writer.write(DATABASEPATH, ints);

         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object.getClass().isArray());
         int[] arr = (int[]) object;
         Assertions.assertEquals(ints.length, arr.length);
         for (int i = 0; i < ints.length; i++)
         {
            Assertions.assertEquals(ints[i], arr[i]);
         }
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
   
   @Test
   public void T2_SimpleStringArrayCreationTest()
   {
      try
      {
         ArrayObject1 object = new ArrayObject1();
         object.setArray(ARRAY1);
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T3_SimpleStringArrayReadingTest()
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ArrayObject1);
         String[] arr = ((ArrayObject1)object).getArray();
         Assertions.assertEquals(3, arr.length);
         Assertions.assertEquals(VALUE1, arr[0]);
         Assertions.assertEquals(VALUE2, arr[1]);
         Assertions.assertEquals(VALUE3, arr[2]);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
   
   @Test
   public void T4_SimpleValuesArrayCreationTest()
   {
      try
      {
         ArrayObject2 object = new ArrayObject2();
         object.setIntArray(INTARRAY1);
         object.setFloatArray(FLOATARRAY1);
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T5_SimpleValuesArrayReadingTest()
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ArrayObject2);
         
         int[] iarr = ((ArrayObject2)object).getIntArray();
         Assertions.assertEquals(4, iarr.length);
         Assertions.assertEquals(4, iarr[0]);
         Assertions.assertEquals(5, iarr[1]);
         Assertions.assertEquals(6, iarr[2]);
         Assertions.assertEquals(7, iarr[3]);
         
         float[] farr = ((ArrayObject2)object).getFloatArray();
         Assertions.assertEquals(2, farr.length);
         Assertions.assertTrue(FLOATARRAY1[0] == farr[0]);
         Assertions.assertTrue(FLOATARRAY1[1] == farr[1]);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
   
   @Test
   public void T6_SimpleObjectArrayCreationTest()
   {
      try
      {
         ArrayObject0 object0 = new ArrayObject0(VALUE1);
         ArrayObject0 object1 = new ArrayObject0(VALUE2);
         ArrayObject0 object2 = new ArrayObject0(VALUE3);
         ArrayObject3 object3 = new ArrayObject3();
         ArrayObject0[] arr = { object0, object1, object2 };
         object3.setArray(arr);
         Writer.write(DATABASEPATH, object3);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T7_SimpleObjectArrayReadingTest()
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ArrayObject3);
         ArrayObject0[] arr = ((ArrayObject3)object).getArray();
         Assertions.assertNotNull(arr);
         Assertions.assertEquals(3, arr.length);

         ArrayObject0 object1 = arr[0];
         Assertions.assertNotNull(object1);
         String value1 = object1.getValue();
         Assertions.assertNotNull(value1);
         Assertions.assertEquals(VALUE1, value1);

         ArrayObject0 object2 = arr[1];
         Assertions.assertNotNull(object2);
         String value2 = object2.getValue();
         Assertions.assertNotNull(value2);
         Assertions.assertEquals(VALUE2, value2);

         ArrayObject0 object3 = arr[2];
         Assertions.assertNotNull(object3);
         String value3 = object3.getValue();
         Assertions.assertNotNull(value3);
         Assertions.assertEquals(VALUE3, value3);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
}

class ArrayObject0
{
   private String value = "";
   
   public ArrayObject0() {}
   
   public ArrayObject0(String init)
   {
      value = init;
   }
   
   public String getValue()
   {
      return value;
   }
   public void setValue(String value)
   {
      this.value = value;
   }
}

class ArrayObject1
{
   private String[] arr = new String[0];
   
   public ArrayObject1() {}
   
   public String[] getArray()
   {
      return arr;
   }
   public void setArray(String[] value)
   {
      arr = value;
   }
}

class ArrayObject2
{
   private int[] intArray = new int[0];
   private float[] floatArray = new float[0];
   
   public ArrayObject2() {}
   
   public int[] getIntArray()
   {
      return intArray;
   }
   public void setIntArray(int[] value)
   {
      intArray = value;
   }
   
   public float[] getFloatArray()
   {
      return floatArray;
   }
   public void setFloatArray(float[] value)
   {
      floatArray = value;
   }
}

class ArrayObject3
{
   private ArrayObject0[] arr = new ArrayObject0[0];
   
   public ArrayObject3() {}
   
   public ArrayObject0[] getArray()
   {
      return arr;
   }
   public void setArray(ArrayObject0[] value)
   {
      arr = value;
   }
}

class ArrayObject4
{
   private ArrayObject1[] arr = new ArrayObject1[0];
   
   public ArrayObject4() {}
   
   public ArrayObject1[] getArray()
   {
      return arr;
   }
   public void setArray(ArrayObject1[] value)
   {
      arr = value;
   }
}
