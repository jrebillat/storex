package net.alantea.storex.test;

import java.io.File;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class ByteArrayTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T0_ByteArrayCreationTest()
   {
      try
      {
         ByteArrayObject object = new ByteArrayObject();
         byte[] bytes = new byte[10];
         bytes[0] = 'A';
         bytes[1] = 'B';
         bytes[2] = 'C';
         bytes[3] = 'D';
         bytes[4] = 'E';
         bytes[5] = 'F';
         bytes[6] = 'G';
         bytes[7] = 'H';
         bytes[8] = 'I';
         bytes[9] = 'J';
         object.setArray(bytes);
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T1_ByteArrayReadingTest()
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof ByteArrayObject);
         byte[] arr = ((ByteArrayObject)object).getArray();
         Assertions.assertNotNull(arr);
         Assertions.assertEquals(10, arr.length);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
}

class ByteArrayObject
{
   private byte[] arr = new byte[0];

   public ByteArrayObject() {}

   public byte[] getArray()
   {
      return arr;
   }
   public void setArray(byte[] value)
   {
      arr = value;
   }
}
