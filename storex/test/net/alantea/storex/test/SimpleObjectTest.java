package net.alantea.storex.test;

import java.io.File;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class SimpleObjectTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String STRINGCONTENT = "TestString\nWith several lines\nin it.";
   private static final int INTCONTENT = 123456;
   private static final long LONGCONTENT = 123456789L;
   private static final boolean BOOLEANCONTENT = true;
   private static final float FLOATCONTENT = 1.23456789f;
   private static final double DOUBLECONTENT = Math.PI;
   private static final byte BYTECONTENT = (byte) 0xFF;
   private static final short SHORTCONTENT = 1234;

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T1_SimpleObjectWithStringTest() throws StorexException
   {
      SimpleObject1 wobject = new SimpleObject1();
         wobject.setValue(STRINGCONTENT);
         Writer.write(DATABASEPATH, wobject);

         Object robject = (Object) Reader.load(DATABASEPATH);
         Assertions.assertNotNull(robject);
         Assertions.assertTrue(robject instanceof SimpleObject1);
         SimpleObject1 object1 = (SimpleObject1) robject;
         Assertions.assertEquals(STRINGCONTENT, object1.getValue());
   }

   @Test
   public void T2_SimpleObjectWithSeveralTest() throws StorexException
   {
      SimpleObject2 wobject = new SimpleObject2();
      wobject.setStringValue(STRINGCONTENT);
      wobject.setIntValue(INTCONTENT);
      wobject.setLongValue(LONGCONTENT);
      wobject.setFloatValue(FLOATCONTENT);
      wobject.setDoubleValue(DOUBLECONTENT);
      wobject.setByteValue(BYTECONTENT);
      wobject.setShortValue(SHORTCONTENT);
      wobject.setBooleanValue(BOOLEANCONTENT);
      Writer.write(DATABASEPATH, wobject);

      Object robject = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(robject);
      Assertions.assertTrue(robject instanceof SimpleObject2);
      SimpleObject2 object2 = (SimpleObject2) robject;
      Assertions.assertEquals(STRINGCONTENT, object2.getStringValue());
      Assertions.assertEquals(INTCONTENT, object2.getIntValue());
      Assertions.assertEquals(LONGCONTENT, object2.getLongValue());
      Assertions.assertEquals(BOOLEANCONTENT, object2.getBooleanValue());
      Assertions.assertTrue(FLOATCONTENT == object2.getFloatValue());
      Assertions.assertTrue(DOUBLECONTENT == object2.getDoubleValue());
      Assertions.assertEquals(SHORTCONTENT, object2.getShortValue());
      Assertions.assertEquals(BYTECONTENT, object2.getByteValue());
   }

   @Test
   public void T3_SimpleObjectWithMultipleTest() throws StorexException
   {
      SimpleObject3 wobject = new SimpleObject3();
      wobject.setValue1("test 1");
      wobject.setValue2("test 2");
      wobject.setValue3("test 3");
      Writer.write(DATABASEPATH, wobject);

      Object robject = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(robject);
      Assertions.assertTrue(robject instanceof SimpleObject3);
      SimpleObject3 object2 = (SimpleObject3) robject;
      Assertions.assertEquals("test 1", object2.getValue1());
      Assertions.assertEquals("test 2", object2.getValue2());
      Assertions.assertEquals("test 3", object2.getValue3());
   }
   
   @Test
   public void T4_SimpleDerivedObjectTest() throws StorexException
   {
      SimpleObject4 wobject = new SimpleObject4();
      wobject.setValue(STRINGCONTENT);
      Writer.write(DATABASEPATH, wobject);

      Object robject = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(robject);
      Assertions.assertTrue(robject instanceof SimpleObject4);
      SimpleObject4 object1 = (SimpleObject4) robject;
      Assertions.assertEquals(STRINGCONTENT, object1.getValue());
   }
}

class SimpleObject1
{
   private String value;
   
   public SimpleObject1() {}
   
   public String getValue()
   {
      return value;
   }
   public void setValue(String value)
   {
      this.value = value;
   }
}

class SimpleObject2
{
   private String stringValue;
   private int intValue;
   private long longValue;
   private float floatValue;
   private double doubleValue;
   private byte byteValue;
   private short shortValue;
   private boolean booleanValue;
   
   public SimpleObject2() {}
   
   public String getStringValue()
   {
      return stringValue;
   }
   public void setStringValue(String value)
   {
      this.stringValue = value;
   }

   public int getIntValue()
   {
      return intValue;
   }

   public void setIntValue(int intValue)
   {
      this.intValue = intValue;
   }

   public long getLongValue()
   {
      return longValue;
   }

   public void setLongValue(long longValue)
   {
      this.longValue = longValue;
   }

   public boolean getBooleanValue()
   {
      return booleanValue;
   }

   public void setBooleanValue(boolean booleanValue)
   {
      this.booleanValue = booleanValue;
   }

   public float getFloatValue()
   {
      return floatValue;
   }

   public void setFloatValue(float floatValue)
   {
      this.floatValue = floatValue;
   }

   public double getDoubleValue()
   {
      return doubleValue;
   }

   public void setDoubleValue(double doubleValue)
   {
      this.doubleValue = doubleValue;
   }

   public byte getByteValue()
   {
      return byteValue;
   }

   public void setByteValue(byte byteValue)
   {
      this.byteValue = byteValue;
   }

   public short getShortValue()
   {
      return shortValue;
   }

   public void setShortValue(short shortValue)
   {
      this.shortValue = shortValue;
   }
}

class SimpleObject3
{
   private String value1;
   private String value2;
   private String value3;
   
   public SimpleObject3() {}
   
   public String getValue1()
   {
      return value1;
   }
   public void setValue1(String value)
   {
      this.value1 = value;
   }
   
   public String getValue2()
   {
      return value2;
   }
   public void setValue2(String value)
   {
      this.value2 = value;
   }
   
   public String getValue3()
   {
      return value3;
   }
   public void setValue3(String value)
   {
      this.value3 = value;
   }
}

class SimpleObject4 extends SimpleObject1
{
   public SimpleObject4() {}
}
