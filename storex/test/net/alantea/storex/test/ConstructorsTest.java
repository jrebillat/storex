package net.alantea.storex.test;

import java.io.File;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;
import org.xml.sax.Attributes;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class ConstructorsTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T01_ObjectWithEmptyConstructorTest() throws StorexException
   {
      ConstructorObject0 wobject = new ConstructorObject0();
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject0);
   }
   
   @Test
   public void T02_ObjectWithFatherTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject1 wobject = new ConstructorObject1(wholder);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject1);
   }
   
   @Test
   public void T03_ObjectWithMapTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject3 wobject = new ConstructorObject3(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject3);
   }
   
   @Test
   public void T04_ObjectWithFatherAndMapTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject4 wobject = new ConstructorObject4(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject4);
   }
   
   @Test
   public void T05_ObjectWithEmptyAndFatherMapTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject5 wobject = new ConstructorObject5(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject5);
   }
   
   @Test
   public void T06_ObjectWithEmptyAndMapTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject6 wobject = new ConstructorObject6(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject6);
   }
   
   @Test
   public void T07_ObjectWithEmptyAndFatherTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject8 wobject = new ConstructorObject8(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject8);
   }
   
   @Test
   public void T08_ObjectWithFatherAndAttributesTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject9 wobject = new ConstructorObject9(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject9);
   }
   
   @Test
   public void T09_ObjectWithFatherAndMapTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject10 wobject = new ConstructorObject10(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject10);
   }
   
   @Test
   public void T10_ObjectWithFatherAndBothTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject11 wobject = new ConstructorObject11(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject11);
   }
   
   @Test
   public void T11_ObjectWithMapAndBothTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject12 wobject = new ConstructorObject12(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject12);
   }
   
   @Test
   public void T12_ObjectWithAttributesAndBothTest() throws StorexException
   {
      ConstructorObjectHolder wholder = new ConstructorObjectHolder();
      ConstructorObject12 wobject = new ConstructorObject12(0);
      wholder.setContent(wobject);
      Writer.write(DATABASEPATH, wholder);

      Object rholder = (Object) Reader.load(DATABASEPATH);
      Assertions.assertNotNull(rholder);
      Assertions.assertTrue(rholder instanceof ConstructorObjectHolder);
      Object content = ((ConstructorObjectHolder) rholder).getContent();
      Assertions.assertNotNull(content);
      Assertions.assertTrue(content instanceof ConstructorObject12);
   }
}

class ConstructorObjectHolder
{
   Object content;
   
   public ConstructorObjectHolder() {}
   
   public Object getContent()
   {
      return content;
   }
   
   public void setContent(Object object)
   {
      content = object;
   }
}

class ConstructorObject0
{
   public ConstructorObject0() {}
}

class ConstructorObject1
{
   public ConstructorObject1(ConstructorObjectHolder father) {}
}

class ConstructorObject2
{
   public ConstructorObject2(int value) {}
   public ConstructorObject2(Attributes attributes) {}
}

class ConstructorObject3
{
   public ConstructorObject3(int value) {}
   public ConstructorObject3(Map<String, String> attributes) {}
}

class ConstructorObject4
{
   public ConstructorObject4(int value) {}
   public ConstructorObject4(ConstructorObjectHolder father, Map<String, String> attributes) {}
}

class ConstructorObject5
{
   public ConstructorObject5() throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject5(int value) {}
   public ConstructorObject5(ConstructorObjectHolder father, Map<String, String> attributes) {}
}

class ConstructorObject6
{
   public ConstructorObject6() throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject6(int value) {}
   public ConstructorObject6(Map<String, String> attributes) {}
}

class ConstructorObject7
{
   public ConstructorObject7() throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject7(int value) {}
   public ConstructorObject7(Attributes attributes) {}
}

class ConstructorObject8
{
   public ConstructorObject8() throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject8(int value) {}
   public ConstructorObject8(ConstructorObjectHolder father) {}
}

class ConstructorObject9
{
   public ConstructorObject9(Attributes attributes) throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject9(int value) {}
   public ConstructorObject9(ConstructorObjectHolder father) {}
}

class ConstructorObject10
{
   public ConstructorObject10(Map<String, String> attributes) throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject10(int value) {}
   public ConstructorObject10(ConstructorObjectHolder father) {}
}

class ConstructorObject11
{
   public ConstructorObject11(ConstructorObjectHolder father) throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject11(int value) {}
   public ConstructorObject11(ConstructorObjectHolder father, Map<String, String> attributes) {}
}

class ConstructorObject12
{
   public ConstructorObject12(Map<String, String> attributes) throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject12(int value) {}
   public ConstructorObject12(ConstructorObjectHolder father, Map<String, String> attributes) {}
}

class ConstructorObject13
{
   public ConstructorObject13(Attributes attributes) throws Exception {throw new Exception("Error, using bad constructor !");}
   public ConstructorObject13(int value) {}
   public ConstructorObject13(ConstructorObjectHolder father, Map<String, String> attributes) {}
}