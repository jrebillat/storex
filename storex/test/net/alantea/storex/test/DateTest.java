package net.alantea.storex.test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class DateTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   
   private static final Date date = new Date(13456789);

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T1_DateCreationTest()
   {
      try
      {
         Writer.write(DATABASEPATH, date);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T2_DateReadingTest() throws IOException
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof Date);
         Date newDate = (Date)object;
         Assertions.assertEquals(date, newDate);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
   
   @Test
   public void T3_DateObjectCreationTest()
   {
      try
      {
         T0_clear();
         MyDateObject object = new MyDateObject();
         object.setDate(date);
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T4_DateObjectReadingTest() throws IOException
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof MyDateObject);
         MyDateObject newDateObject = (MyDateObject)object;
         Date newDate = newDateObject.getDate();
         Assertions.assertNotNull(newDate);
         Assertions.assertEquals(date, newDate);
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
}

class MyDateObject
{
   Date date;

   public MyDateObject() {}

   public Date getDate()
   {
      return date;
   }

   public void setDate(Date date)
   {
      this.date = date;
   }
}
