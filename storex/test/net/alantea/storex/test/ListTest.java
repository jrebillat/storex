package net.alantea.storex.test;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class ListTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String LISTCONTENT1 = "TestString1";
   private static final String LISTCONTENT2 = "This is a test string";
   private static final String LISTCONTENT3 = "TestString3";

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test 
   public void T1_ListTest() throws StorexException
   {
      List<Object> wlist = new ArrayList<>();
      wlist.add(LISTCONTENT1);
      wlist.add(LISTCONTENT2);
      wlist.add(LISTCONTENT3);
      Writer.write(DATABASEPATH, wlist);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof List);
      @SuppressWarnings("unchecked")
      List<Object> rlist = (List<Object>) object;
      Assertions.assertEquals(3, rlist.size());
      Assertions.assertEquals(LISTCONTENT1, rlist.get(0));
      Assertions.assertEquals(LISTCONTENT2, rlist.get(1));
      Assertions.assertEquals(LISTCONTENT3, rlist.get(2));
   }
   
   @Test 
   public void T2_EmptyListTest() throws StorexException
   {
      List<Object> wlist = new ArrayList<>();
      Writer.write(DATABASEPATH, wlist);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof List);
      @SuppressWarnings("unchecked")
      List<Object> rlist = (List<Object>) object;
      Assertions.assertTrue(rlist.isEmpty());
   }
   
   @Test 
   public void T3_ObjectWithListTest() throws StorexException
   {
      ListObject1 wlist = new ListObject1();
      wlist.addValue(LISTCONTENT1);
      wlist.addValue(LISTCONTENT2);
      wlist.addValue(LISTCONTENT3);
      Writer.write(DATABASEPATH, wlist);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof ListObject1);
      ListObject1 robject = (ListObject1) object;
      List<String> rlist = robject.getValues();
      Assertions.assertEquals(3, rlist.size());
      Assertions.assertEquals(LISTCONTENT1, rlist.get(0));
      Assertions.assertEquals(LISTCONTENT2, rlist.get(1));
      Assertions.assertEquals(LISTCONTENT3, rlist.get(2));
   }
   
   @Test 
   public void T4_ObjectWithEmptyListTest() throws StorexException
   {
      ListObject1 wlist = new ListObject1();
      Writer.write(DATABASEPATH, wlist);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof ListObject1);
      ListObject1 robject = (ListObject1) object;
      List<String> rlist = robject.getValues();
      Assertions.assertTrue(rlist.isEmpty());
   }
   
   @Test 
   public void T4_ObjectWithListsTest() throws StorexException
   {
      ListObject2 wlist = new ListObject2();
      wlist.addValue1(LISTCONTENT1);
      wlist.addValue1(LISTCONTENT2);
      wlist.addValue1(LISTCONTENT3);
      wlist.addValue2(LISTCONTENT3);
      wlist.addValue2(LISTCONTENT2);
      Writer.write(DATABASEPATH, wlist);

      Object object = Reader.load(DATABASEPATH);
      Assertions.assertNotNull(object);
      Assertions.assertTrue(object instanceof ListObject2);
      ListObject2 robject = (ListObject2) object;
      List<String> rlist1 = robject.getValues1();
      Assertions.assertEquals(3, rlist1.size());
      Assertions.assertEquals(LISTCONTENT1, rlist1.get(0));
      Assertions.assertEquals(LISTCONTENT2, rlist1.get(1));
      Assertions.assertEquals(LISTCONTENT3, rlist1.get(2));
      List<String> rlist2 = robject.getValues2();
      Assertions.assertEquals(2, rlist2.size());
      Assertions.assertEquals(LISTCONTENT3, rlist2.get(0));
      Assertions.assertEquals(LISTCONTENT2, rlist2.get(1));
   }
}

class ListObject1
{
   private List<String> values = new LinkedList<>();
   
   public ListObject1() {}
   
   public List<String> getValues()
   {
      return values;
   }
   public void addValue(String value)
   {
      this.values.add(value);
   }
}

class ListObject2
{
   private List<String> values1 = new LinkedList<>();
   private List<String> values2 = new LinkedList<>();
   
   public ListObject2() {}
   
   public List<String> getValues1()
   {
      return values1;
   }
   public void addValue1(String value)
   {
      this.values1.add(value);
   }
   
   public List<String> getValues2()
   {
      return values2;
   }
   public void addValue2(String value)
   {
      this.values2.add(value);
   }
}