package net.alantea.storex.test;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.Handler;
import net.alantea.storex.handlers.HandlerException;
import net.alantea.storex.handlers.Names;

@TestMethodOrder(MethodName.class)
public class NewHandlerTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";
   private static final String STRINGCONTENT = "My string value";

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T1_SimpleHandlerTest() throws StorexException
   {
      Handler.prependHandler(NewHandler1.class);
      HandledObject1 wobject = new HandledObject1();
         wobject.setValue(STRINGCONTENT);
         Writer.write(DATABASEPATH, wobject);

         Object robject = (Object) Reader.load(DATABASEPATH);
         Assertions.assertNotNull(robject);
         Assertions.assertTrue(robject instanceof HandledObject1);
         HandledObject1 object1 = (HandledObject1) robject;
         Assertions.assertEquals(STRINGCONTENT, object1.getValue());
   }
}

class HandledObject1
{
   private transient String value;
   
   public HandledObject1() {}
   
   public String getValue()
   {
      return value;
   }
   public void setValue(String value)
   {
      this.value = value;
   }
}

class NewHandler1 extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CONTENTTAG, true);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, true);
  }};

   public NewHandler1(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      String content = attributes.get(Names.CONTENTTAG);
      HandledObject1 handled = new HandledObject1();
      handled.setValue(content);
      setTarget(handled, father, field);
   }
   
   public NewHandler1(Object target)
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      write("Handled1", name,Names.CONTENTTAG, ((HandledObject1) target).getValue());
   }
   
   /**
    * On end.
    *
    * @throws HandlerException the handler exception
    */
   @Override
   public void onEnd(String characters) throws HandlerException
   {
      // default : empty
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return (HandledObject1.class.equals(target.getClass()));
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return "Handled1".equals(type);
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}