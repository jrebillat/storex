package net.alantea.storex.test;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({ 
   SimpleContentTest.class,
   SimpleObjectTest.class,
   ConstructorsTest.class,
   ListTest.class,
   ArrayTest.class,
   MapTest.class,
   ComposedObjectsTest.class,
   PropertiesTest.class,
   SpecialObjectsTest.class,
   NewHandlerTest.class,
   ByteArrayTest.class
   })
public class AllTests
{
}
