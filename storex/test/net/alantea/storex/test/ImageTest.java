package net.alantea.storex.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.storex.Reader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;

@TestMethodOrder(MethodName.class)
public class ImageTest
{
   private static final String DATABASEPATH = "OpenAndCloseTestDatabase";

   @Test
   public void T0_clear()
   {
      File file = new File(DATABASEPATH);
      if (file.exists())
      {
         file.delete();
      }
   }
   
   @Test
   public void T0_ImageCreationTest()
   {
      try
      {
         BufferedImage object = ImageIO.read(new File("sample.png"));
         Writer.write(DATABASEPATH, object);
      }
      catch (StorexException | IOException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }

   @Test
   public void T1_ImageReadingTest() throws IOException
   {
      try
      {
         Object object = Reader.load(DATABASEPATH);
         Assertions.assertNotNull(object);
         Assertions.assertTrue(object instanceof BufferedImage);
         BufferedImage reference = ImageIO.read(new File("sample.png"));
         Assertions.assertTrue(compareImage(reference, (BufferedImage) object));
      }
      catch (StorexException e)
      {
         e.printStackTrace();
         Assertions.fail(e.getMessage());
      }
   }
   
   public static boolean compareImage(BufferedImage reference, BufferedImage object) {        
      try {
         DataBuffer dbA = reference.getData().getDataBuffer();
         int sizeA = dbA.getSize();                   
         DataBuffer dbB = object.getData().getDataBuffer();
         int sizeB = dbB.getSize();
         // compare data-buffer objects //
         if(sizeA == sizeB) {
             for(int i=0; i<sizeA; i++) { 
                 if(dbA.getElem(i) != dbB.getElem(i)) {
                     return false;
                 }
             }
             return true;
         }
         else {
             return false;
         }
     } 
     catch (Exception e) { 
         System.out.println("Failed to compare image files ...");
         return  false;
     }
 }
}
