package net.alantea.storex;

import net.alantea.utils.exception.LntException;

/**
 * The Class StorexException.
 */
public class StorexException extends LntException
{
   private static final long serialVersionUID = 1L;

   /**
    * Instantiates a new storex exception.
    *
    * @param message the message
    */
   public StorexException(String message)
   {
      super(message);
   }

   /**
    * Instantiates a new storex exception.
    *
    * @param cause the cause
    */
   public StorexException(Throwable cause)
   {
      super(cause.getMessage(), cause);
   }

   /**
    * Instantiates a new storex exception.
    *
    * @param message the message
    * @param cause the cause
    */
   public StorexException(String message, Throwable cause)
   {
      super(message, cause);
   }
}
