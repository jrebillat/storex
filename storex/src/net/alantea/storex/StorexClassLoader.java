package net.alantea.storex;
import java.io.File;
import java.net.URL;

import net.alantea.storex.handlers.ObjectHandler;
import net.alantea.utils.EnhancedClassLoader;

/**
 * The Class StorexClassHandler.
 */
public final class StorexClassLoader extends EnhancedClassLoader 
{
   
   /** The instance. */
   private static StorexClassLoader instance;
   
   static
   {
      instance = new StorexClassLoader(ObjectHandler.class.getClassLoader());
   }
   
   /**
    * Instantiates a new storex class handler.
    *
    * @param parent the parent
    */
   private StorexClassLoader(ClassLoader parent)
   {
      super(parent);
   }
   
   /**
    * Gets the single instance of StorexClassHandler.
    *
    * @return single instance of StorexClassHandler
    */
   public static StorexClassLoader getInstance()
   {
      return instance;
   }

   /**
    * Adds the jar file.
    *
    * @param url the url
    */
   public static void addJarFile(URL url)
   {
       instance.addClassesFile(url);
   }

   /**
    * Adds the jar file.
    *
    * @param file the file
    */
   public static void addJarFile(File file)
   {
      getInstance().addClassesFile(file);
   }
}
