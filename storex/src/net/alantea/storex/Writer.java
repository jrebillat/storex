package net.alantea.storex;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import net.alantea.storex.handlers.Handler;

/**
 * The Class Writer.
 */
public class Writer
{
   private static XMLStreamWriter writer;
   
   private static Map<String, String> placeholders;
   
//   private static int count;
   
   /**
    * Write an object and its content.
    *
    * @param path the path
    * @param object the object
    * @throws StorexException the storex exception
    */
   public static void write(String path, Object object) throws StorexException
   {
	   write(path, object, null);
   }
   
   /**
    * Write an object and its content with placeholders.
    *
    * @param path the path
    * @param object the object
    * @param replacements the replacements
    * @throws StorexException the storex exception
    */
   public static void write(String path, Object object, Map<String, String> replacements) throws StorexException
   {
      write(path, object, replacements, null);
   }
   
   /**
    * Write an object and its content with placeholders and key/values.
    *
    * @param path the path
    * @param object the object
    * @param replacements the replacements
    * @param keyValues the key values
    * @throws StorexException the storex exception
    */
   public static void write(String path, Object object, Map<String, String> replacements, Map<String, String> keyValues) throws StorexException
   {
      XMLOutputFactory f = XMLOutputFactory.newInstance();
      try
      {
         BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(path));
         writer = f.createXMLStreamWriter(fw, "UTF-8");
         writer.writeStartDocument("UTF-8", "1.0");
         
         addPlaceholders(replacements);
         addKeyValues(replacements);
         
         createElement(null, null, object);
         writer.writeEndDocument();
         writer.flush();
         fw.flush();
         writer.close();
         fw.close();
      }
      catch (XMLStreamException | IOException e1)
      {
         throw new StorexException(e1);
      }
   }

   /**
    * Adds the placeholders.
    *
    * @param replacements the replacements
    * @throws XMLStreamException the XML stream exception
    */
   private static void addPlaceholders(Map<String, String> replacements) throws XMLStreamException
   {
	   placeholders = new HashMap<>();

	   if (replacements != null)
	   {
	      for (String key : replacements.keySet())
	      {
			  placeholders.put(replacements.get(key), key);
		      writer.writeProcessingInstruction("placeholder", key + "=" + replacements.get(key));
	      }
	   }
   }

   /**
    * Adds the key and values.
    *
    * @param keyValues the key and values
    * @throws XMLStreamException the XML stream exception
    */
   private static void addKeyValues(Map<String, String> keyValues) throws XMLStreamException
   {
      if (keyValues != null)
      {
         for (String key : keyValues.keySet())
         {
            writer.writeProcessingInstruction("keyvalues", key + "=" + keyValues.get(key));
         }
      }
   }

   /**
    * Creates the element.
    *
    * @param field the field
    * @param name the name
    * @param target the target
    * @throws StorexException the storex exception
    */
   public static void createElement(Field field, String name, Object target) throws StorexException
   {
      if (target != null)
      {
         Handler handler = Handler.loadHandler(target);
         if (handler != null)
         {
            handler.createElement(target, (field == null) ? null : field.getName());
         }
      }
   }

   public static void writeEmptyElement(String name) throws StorexException
   {
      try
      {
         writer.writeEmptyElement(name);
      }
      catch (XMLStreamException e)
      {
         throw new StorexException(e);
      }
   }

   public static void writeStartElement(String name) throws StorexException
   {
      try
      {
//         writer.writeCharacters("\n");
//         for (int i = 0; i < count; i++)
//         {
//            writer.writeCharacters("  ");
//         }
//         count++;
         writer.writeStartElement(name);
      }
      catch (XMLStreamException e)
      {
         throw new StorexException(e);
      }
   }

   public static void writeAttribute(String name, String value) throws StorexException
   {
	   
	   String toWrite = placeholders.get(value);
	   if (toWrite == null)
	   {
		   toWrite = value;
	   }
      try
      {
         writer.writeAttribute(name, toWrite);
      }
      catch (XMLStreamException e)
      {
         throw new StorexException(e);
      }
   }

   public static void writeEndElement() throws StorexException
   {
      try
      {
//         count--;
//         writer.writeCharacters("\n");
//         for (int i = 0; i < count; i++)
//         {
//            writer.writeCharacters("  ");
//         }
         writer.writeEndElement();
      }
      catch (XMLStreamException e)
      {
         throw new StorexException(e);
      }
   }

   public static void writeCharacters(String characters) throws StorexException
   {
      try
      {
         writer.writeCData(characters);
      }
      catch (XMLStreamException e)
      {
         throw new StorexException(e);
      }
   }
}
