package net.alantea.storex;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.alantea.storex.handlers.Handler;
import net.alantea.storex.handlers.HandlerException;

/**
 * The Class Reader.
 */
public class Reader extends DefaultHandler
{
   /** The stack. */
   private Stack<Handler> stack = new Stack<>();
   
   /** version placeholders list. */
   private static Map<String, String> versionPlaceholders = new HashMap<>();
   
   /** placeholders list. */
   private static Map<String, String> placeholders = new HashMap<>();
   
   /** keyvalues list. */
   private static Map<String, String> keyvalues = new HashMap<>();
   
   /** The parser. */
   private SAXParser parser;
   
   /**  The root object. */
   private Object root;
   
   /**
    * Instantiates a new reader.
    * @throws StorexException 
    *
    */
   private Reader() throws StorexException
   {      
      SAXParserFactory spfac = SAXParserFactory.newInstance();

      try
      {
         parser = spfac.newSAXParser();
      }
      catch (ParserConfigurationException | SAXException e)
      {
         throw new StorexException(e);
      }
   }

/**
    * load a file.
    *
    * @param path the path
    * @return the object
    * @throws StorexException the storex exception
    */
   public static Object load(String path) throws StorexException
   {
      return load(new File(path));
   }

   /**
    * load a file.
    *
    * @param reader the reader
    * @return the object
    * @throws StorexException the storex exception
    */
   public static Object load(java.io.Reader reader) throws StorexException
   {
      return load(new InputSource(reader));
   }

   /**
    * load a file.
    *
    * @param file the file
    * @return the object
    * @throws StorexException the storex exception
    */
   public static Object load(File file) throws StorexException
   {
      Object ret = null;
      try
      {
         if (file.exists())
         {
            FileInputStream stream = new FileInputStream(file);
            InputSource source = new InputSource(stream);
            ret = load(source);
            stream.close();
            return ret;
         }
      }
      catch (IOException e)
      {
         throw new StorexException(e);
      }
      return ret;
   }

   /**
    * load a file.
    *
    * @param source the source
    * @return the object
    * @throws StorexException the storex exception
    */
   public static Object load(InputSource source) throws StorexException
   {
      try
      {
         keyvalues.clear();
         Reader reader = new Reader();
         reader.parser.parse(source, reader);
         return reader.getRootInformation();
      }
      catch (IOException | SAXException e)
      {
         throw new StorexException(e);
      }
   }
   
   public static void addVersionPlaceHolder(String oldString, String newString)
   {
      versionPlaceholders.put(oldString, newString);
   }
   
   public static void addPlaceHolder(String oldString, String newString)
   {
      placeholders.put(oldString, newString);
   }
   
   public static String getKeyValue(String key)
   {
      return keyvalues.get(key);
   }
   
   /**
    * Gets the keys.
    *
    * @return the keys
    */
   public static List<String> getKeys()
   {
      List<String> ret = new LinkedList<>();
      ret.addAll(keyvalues.keySet());
      return ret;
   }

   /**
    * Gets the root information.
    *
    * @return the root information
    */
   private Object getRootInformation()
   {
      return root;
   }

   /* (non-Javadoc)
    * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
    */
   @Override
   public final void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
   {
	   Map<String, String> attrs = new HashMap<>();
	   for (int i = 0; i < attributes.getLength(); i++)
	   {
		   String key = attributes.getQName(i);
         String toPut = placeholders.get(attributes.getValue(i));
         if (toPut == null)
         {
            toPut = attributes.getValue(i);
         }
         if (toPut.equals("net.alantea.glideui.bdd.Configuration"))
         {
            System.out.println("Tutu");
         }
         String toPut1 = versionPlaceholders.get(toPut);
         if (toPut1 != null)
         {
            toPut = toPut1;
         }
         attrs.put(key, toPut);
	   }
      Handler father = null;
      Object fatherTarget = null;
      if (!stack.isEmpty())
      {
         father = stack.peek();
      }
      String fieldName = attrs.get("field");
      Field field = null;
      if (father != null)
      {
         fatherTarget = father.getTarget();
         if (fatherTarget != null)
         {
            Class<?> cl = fatherTarget.getClass();
            while ((field == null) && (!Object.class.equals(cl)))
            {
               try
               {
                  if (fieldName != null)
                  {
                     String realName = placeholders.get(fieldName);
                     if (realName == null)
                     {
                        realName = fieldName;
                     }
                     field = cl.getDeclaredField(realName);
                  }
               }
               catch (SecurityException | NoSuchFieldException e)
               {
                  // nothing
               }
               cl = cl.getSuperclass();
            }
         }
      }
      Handler handler;
      try
      {
         handler = Handler.loadHandler(fatherTarget, field, qName, attrs);
      }
      catch (HandlerException e)
      {
         throw new SAXException(e);
      }
      if (handler != null)
      {
         stack.push(handler);
      }
      else
      {
         throw new SAXException(new NullPointerException());
      }
   }
   
   /* (non-Javadoc)
    * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
    */
   public void characters(char ch[],int start,int length)
   {
      if (!stack.isEmpty())
      {
         Handler target = stack.peek();
         target.addCharacters(ch, start, length);
//  	      int realStart = start;
//  	      int realLength = length;
//  	   
//         while ((ch[realStart] == '\n') || (ch[realStart] == ' '))
//         {
//            realStart++;
//            realLength--;
//         }
//         while ((ch[realStart + realLength - 1] == '\n') || (ch[realStart + realLength - 1] == ' '))
//         {
//            realLength--;
//         }
//         if (realLength > 0)
//         {
//            target.addCharacters(ch, realStart, realLength);
//         }
      }
   } 

   /* (non-Javadoc)
    * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
    */
   @Override
   public final void endElement(String uri, String localName, String qName) throws SAXException
   {
      try
      {
         Handler target = null;
         if (!stack.isEmpty())
         {
            target = stack.pop();
            target.onEnd(target.getBufferContent());
            if (stack.isEmpty())
            {
               root = target.getTarget();
            }
            else
            {
               Handler father = stack.peek();
               if (father != null)
               {
                  father.insert(target);
               }
            }
         }
      }
      catch (HandlerException e)
      {
         throw new SAXException(e);
      }
   }

   /* (non-Javadoc)
    * @see org.xml.sax.helpers.DefaultHandler#processingInstruction(java.lang.String, java.lang.String)
    */
   @Override
   public final void processingInstruction(String target, String data)
   {
	   if ("placeholder".equals(target))
	   {
		   String[] keyValue = data.split("=");
		   if (keyValue.length == 2)
		   {
			   placeholders.put(keyValue[0], keyValue[1]);
		   }
	   }
	   else if ("keyvalue".equals(target))
      {
         String[] keyValue = data.split("=");
         if (keyValue.length == 2)
         {
            keyvalues.put(keyValue[0], keyValue[1]);
         }
      }
   }
}
