package net.alantea.storex;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Stack;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class StorexStreamWriter implements XMLStreamWriter
{
   private java.io.Writer outputStream;
   
   private CharsetEncoder encoder = null;
   
   private Stack<String> namesStack = new Stack<>();
   private boolean inElementStart = false;

   private boolean emptyElement;

   public StorexStreamWriter(OutputStream output)
   {
      this(new OutputStreamWriter(output));
   }

   public StorexStreamWriter(java.io.Writer output)
   {
      outputStream = output;
      encoder = Charset.forName("UTF-8").newEncoder();
   }
   
   private void writeIndentation() throws IOException
   {
      for (int i = 0; i < namesStack.size(); i++)
      {
         outputStream.write("  ");
      }
   }

   private void openStartElement(String localName) throws IOException
   {
      writeIndentation();
      outputStream.write("<" + localName);
      inElementStart = true;
   }

   private void closeStartElement() throws IOException
   {
      inElementStart = false;
      if (emptyElement)
      {
         outputStream.write(" />\n");
      }
      else
      {
         outputStream.write(">\n");
      }
   }

   @Override
   public void writeStartElement(String localName) throws XMLStreamException
   {
      emptyElement = false;
      try 
      {
         if (localName == null)
         {
            throw new XMLStreamException("Local name cannot be null");
         }

         if (inElementStart)
         {
            closeStartElement();
         }

         openStartElement(localName);
         namesStack.push(localName);
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException
   {
      writeStartElement(localName);
   }

   @Override
   public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException
   {
      writeStartElement(localName);
   }

   @Override
   public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException
   {
      writeEmptyElement(localName);
   }

   @Override
   public void writeEmptyElement(String prefix, String localName, String namespaceURI) throws XMLStreamException
   {
      writeEmptyElement(localName);
   }

   @Override
   public void writeEmptyElement(String localName) throws XMLStreamException
   {
      writeStartElement(localName);
      emptyElement = true;
   }

   @Override
   public void writeEndElement() throws XMLStreamException
   {
      try
      {
         if (inElementStart)
         {
            closeStartElement();
         }

         String localName = namesStack.pop();
         if (emptyElement == false)
         {
            writeIndentation();

            outputStream.write("</");
            outputStream.write(localName);
            outputStream.write(">\n");
         }
         emptyElement = false;
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }

   }

   @Override
   public void writeEndDocument() throws XMLStreamException
   {
      try
      {
         if (inElementStart)
         {
            closeStartElement();
         }
         while (!namesStack.isEmpty())
         {
            writeEndElement();
         }
         outputStream.write("\n");
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void close() throws XMLStreamException
   {
      try
      {
         outputStream.close();
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void flush() throws XMLStreamException
   {
      try
      {
         outputStream.flush();
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   private void writeXMLContent(String content) throws IOException
   {
      for (int i = 0; i < content.length(); i++)
      {
         char ch = content.charAt(i);

         if (encoder != null && !encoder.canEncode(ch))
         {
            outputStream.write("&#x");
            outputStream.write(Integer.toHexString(ch));
            outputStream.write(';');
            continue;
         }

         switch (ch)
         {
            case '<':
               outputStream.write("&lt;");

               break;

            case '&':
               outputStream.write("&amp;");

               break;

            case '>':
               outputStream.write("&gt;");

            default:
               outputStream.write(ch);

               break;
         }
      }
   }

   @Override
   public void writeAttribute(String localName, String value) throws XMLStreamException
   {
      try
      {
         outputStream.write(" ");
         outputStream.write(localName);
         outputStream.write("=\"");
         writeXMLContent(value);  // true = escapeDoubleQuotes
         outputStream.write("\"");
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void writeAttribute(String prefix, String namespaceURI, String localName, String value)
         throws XMLStreamException
   {
      writeAttribute(localName, value);
   }

   @Override
   public void writeAttribute(String namespaceURI, String localName, String value) throws XMLStreamException
   {
      writeAttribute(localName, value);
   }

   @Override
   public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeComment(String data) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeProcessingInstruction(String target) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeProcessingInstruction(String target, String data) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeCData(String data) throws XMLStreamException
   {
      try
      {
         if (data == null)
         {
            throw new XMLStreamException("cdata cannot be null");
         }

         if (inElementStart)
         {
            closeStartElement();
         }

         outputStream.write("<![CDATA[");
         outputStream.write(data);
         outputStream.write("]]>");
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void writeDTD(String dtd) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeEntityRef(String name) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void writeStartDocument() throws XMLStreamException
   {
      writeStartDocument("1.0");
   }

   @Override
   public void writeStartDocument(String version) throws XMLStreamException
   {
      writeStartDocument(version, "UTF-8");
   }

   @Override
   public void writeStartDocument(String encoding, String version) throws XMLStreamException
   {
      try
      {
         outputStream.write("<?xml version=\"" + version + "\" encoding=\"" + encoding + "\"?>\n");
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void writeCharacters(String text) throws XMLStreamException
   {
      try
      {
         if (text == null)
         {
            throw new XMLStreamException("text cannot be null");
         }

         if (inElementStart)
         {
            closeStartElement();
         }

         writeXMLContent(text);
      }
      catch (IOException e)
      {
         throw new XMLStreamException(e);
      }
   }

   @Override
   public void writeCharacters(char[] text, int start, int len) throws XMLStreamException
   {
      writeCharacters(new String(text, start, len));
   }

   @Override
   public String getPrefix(String uri) throws XMLStreamException
   {
      // Auto-generated method stub
      return null;
   }

   @Override
   public void setPrefix(String prefix, String uri) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void setDefaultNamespace(String uri) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
   {
      // Auto-generated method stub

   }

   @Override
   public NamespaceContext getNamespaceContext()
   {
      // Auto-generated method stub
      return null;
   }

   @Override
   public Object getProperty(String name) throws IllegalArgumentException
   {
      // Auto-generated method stub
      return null;
   }

}
