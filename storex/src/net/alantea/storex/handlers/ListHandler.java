package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

public final class ListHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, true);
  }};

   @SuppressWarnings("unchecked")
   public ListHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      List<Object> list = null;
      if ((field != null) && (father != null))
      {
         field.setAccessible(true);
         try
         {
            list = (List<Object>) field.get(father);
         }
         catch (IllegalArgumentException | IllegalAccessException e1)
         {
            e1.printStackTrace();
         }
         if (list == null)
         {
            list = new LinkedList<>();
            try
            {
               field.set(father, list);
            }
            catch (IllegalArgumentException | IllegalAccessException e)
            {
               throw new HandlerException("ListHandler", HandlingMethod.CONSTRUCTOR, e);
            }
         }
      }
      else
      {
         list = new LinkedList<>();
      }
      setTarget(list);
   }

   public ListHandler(Object target) throws HandlerException
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         @SuppressWarnings("unchecked")
         List<Object> list = (List<Object>) target;

         Writer.writeStartElement(Names.LISTTAG);
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         if ((list != null) && (!list.isEmpty()))
         {
            for (Object object : list)
            {
               try
               {
                  Writer.createElement(null, "listelement", object);
               }
               catch (IllegalArgumentException e)
               {
                  e.printStackTrace();
               }
            }
         }
         Writer.writeEndElement();
      }
      catch (IllegalArgumentException e)
      {
         throw new HandlerException("ListHandler", HandlingMethod.WRITING, e);
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("ListHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.LISTTAG.equals(type);
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return target instanceof List;
   }
   
   @Override
   @SuppressWarnings("unchecked")
   public void insert(Handler handler) throws HandlerException
   {
      ((List<Object>)getTarget()).add(handler.getTarget());
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}
