package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import net.alantea.storex.StorexClassLoader;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

public final class EnumHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CONTENTTAG, true);
      put(Names.CLASSTAG, true);
      put(Names.ALLOWOTHERTAGS, false);
  }};

   public EnumHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      String value = attributes.get(Names.CONTENTTAG);
      String className = attributes.get(Names.CLASSTAG);
      Object target = null;
      try
      {
         Class<?> enumClass = StorexClassLoader.getInstance().loadClass(className);
         Object[] objects = enumClass.getEnumConstants();
         for(Object obj : objects)
         {
            Enum<?> e = (Enum<?>) obj;
            if (e.name().equals(value))
            {
               target = obj;
            }
          }
      }
      catch (ClassNotFoundException e)
      {
         throw new HandlerException("EnumHandler", HandlingMethod.CONSTRUCTOR, e);
      }
      setTarget(target, father, field);
   }

   public EnumHandler(Object target) throws HandlerException
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      write(Names.ENUMTAG, name, Names.CONTENTTAG, ((Enum<?>)target).name(), Names.CLASSTAG, target.getClass().getName());
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return (target.getClass().isEnum());
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.ENUMTAG.equals(type);
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}
