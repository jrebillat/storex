package net.alantea.storex.handlers;

import net.alantea.storex.StorexException;

/**
 * The Class HandlerException.
 */
public class HandlerException extends StorexException
{
   
   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 1L;
   
   /**
    * The Enum HandlingMethod.
    */
   public static enum HandlingMethod
   {
      
      /** The constructor. */
      CONSTRUCTOR,
      
      /** The reading. */
      READING,
      
      /** The writing. */
      WRITING,
      
      /** The ending. */
      ENDING,
      
      /** The inserting. */
      INSERTING,
      
      /** The handler. */
      HANDLER,
      
      /** The invoker. */
      INVOKER,
      
      /** The other. */
      OTHER
   }
   
   /** The handler class name. */
   private String handlerClassName;
   
   /** The handler method. */
   private HandlingMethod handlerMethod;

   /**
    * Instantiates a new handler exception.
    *
    * @param who the who
    * @param where the where
    * @param message the message
    */
   public HandlerException(String who, HandlingMethod where, String message)
   {
      super(message);
      handlerClassName = who;
      handlerMethod = where;
   }

   /**
    * Instantiates a new handler exception.
    *
    * @param who the who
    * @param where the where
    * @param cause the cause
    */
   public HandlerException(String who, HandlingMethod where, Throwable cause)
   {
      super(cause);
      handlerClassName = who;
      handlerMethod = where;
   }

   /**
    * Instantiates a new handler exception.
    *
    * @param who the who
    * @param where the where
    * @param message the message
    * @param cause the cause
    */
   public HandlerException(String who, HandlingMethod where,String message, Throwable cause)
   {
      super(message, cause);
      handlerClassName = who;
      handlerMethod = where;
   }

   /**
    * Gets the handler class name.
    *
    * @return the handler class name
    */
   public String getHandlerClassName()
   {
      return handlerClassName;
   }

   /**
    * Gets the handler method.
    *
    * @return the handler method
    */
   public HandlingMethod getHandlerMethod()
   {
      return handlerMethod;
   }
}
