package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

/**
 * The Class ArrayHandler.
 */
public final class ByteArrayHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, false);
  }};

   /**
    * Instantiates a new byte array handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @throws HandlerException the handler exception
    */
   public ByteArrayHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      
//      setTarget(array, father, field);
   }

   /**
    * Instantiates a new array handler.
    *
    * @param target the target
    */
   public ByteArrayHandler(Object target)
   {
      super(target);
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#createElement(org.w3c.dom.Document, java.lang.Object)
    */
   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         Writer.writeStartElement(Names.BYTEARRAYTAG);
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         byte[] bytes = (byte[]) target;
         ;
         Writer.writeCharacters(new String(Base64.getEncoder().encode(bytes)));
         Writer.writeEndElement();
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("ArrayHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   /**
    * Handles read.
    *
    * @param type the type
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.BYTEARRAYTAG.equals(type);
   }

   /**
    * Handles write.
    *
    * @param target the target
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesWrite(Object target) throws HandlerException
   {
      if (target.getClass().isArray())
      {
         Class<?> contentType = target.getClass().getComponentType();
         return "byte".equals(contentType.getName());
      }
      return false;
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#onEnd()
    */
   @Override
   public void onEnd(String characters) throws HandlerException
   {
      byte[] bytes = Base64.getDecoder().decode(characters);
      setTarget(bytes, getFather(), getField());
   }
}
