package net.alantea.storex.handlers;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.StorexClassLoader;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

/**
 * The Class ArrayHandler.
 */
public final class ArrayHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CLASSTAG, true);
      put(Names.SIZETAG, true);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, true);
  }};
   
   /** The index. */
   private int index = 0;
   
   /** The class name. */
   private String className;

   /**
    * Instantiates a new array handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @throws HandlerException the handler exception
    */
   public ArrayHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      className = attributes.get(Names.CLASSTAG);
      Class<?> myClass = null;
      switch (className)
      {
         case "int" :
            myClass = Integer.TYPE;
            break;

         case "long" :
            myClass = Long.TYPE;
            break;

         case "float" :
            myClass = Float.TYPE;
            break;

         case "double" :
            myClass = Double.TYPE;
            break;

         case "short" :
            myClass = Short.TYPE;
            break;

         case "byte" :
            myClass = Byte.TYPE;
            break;
            
         case "boolean" :
            myClass = Boolean.TYPE;
            break;
           
         default :
            try
            {
               myClass = StorexClassLoader.getInstance().loadClass(attributes.get(Names.CLASSTAG));
            }
            catch (ClassNotFoundException e2)
            {
               throw new HandlerException("ArrayHandler", HandlingMethod.CONSTRUCTOR, e2);
            }
      }
      int size = Integer.parseInt(attributes.get(Names.SIZETAG));
      Object array = Array.newInstance(myClass, size);
      setTarget(array, father, field);
   }

   /**
    * Instantiates a new array handler.
    *
    * @param target the target
    */
   public ArrayHandler(Object target)
   {
      super(target);
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#createElement(org.w3c.dom.Document, java.lang.Object)
    */
   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         Writer.writeStartElement(Names.ARRAYTAG);
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         Class<?> contentType = target.getClass().getComponentType();
         List<Object> list = new LinkedList<>();
         switch (contentType.getName())
         {
            case "int" :
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(((int[])target).length));
               for (int v : (int[])target) list.add(Integer.valueOf(v));
               break;

            case "long" :
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(((long[])target).length));
               for (long v : (long[])target) list.add(Long.valueOf(v));
               break;

            case "float" :
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(((float[])target).length));
               for (float v : (float[])target) list.add(Float.valueOf(v));
               break;

            case "double" :
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(((double[])target).length));
               for (double v : (double[])target) list.add(Double.valueOf(v));
               break;

            case "short" :
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(((short[])target).length));
               for (short v : (short[])target) list.add(Short.valueOf(v));
               break;

            case "boolean" :
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(((boolean[])target).length));
               for (byte v : (byte[])target) list.add(Integer.valueOf(v));
               break;

            default :
               Object[] array = (Object[]) target;
               Writer.writeAttribute(Names.SIZETAG, Integer.toString(array.length));
               list = Arrays.asList(array);
         }
         Writer.writeAttribute(Names.CLASSTAG, contentType.getName());
         for (Object object : list)
         {
            Writer.createElement(null, "arrayelement", object);
         }
         Writer.writeEndElement();
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("ArrayHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   /**
    * Handles read.
    *
    * @param type the type
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.ARRAYTAG.equals(type);
   }

   /**
    * Handles write.
    *
    * @param target the target
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesWrite(Object target) throws HandlerException
   {
      return target.getClass().isArray();
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
   
   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#insert(net.alantea.storex.handlers.Handler)
    */
   @Override
   public void insert(Handler handler) throws HandlerException
   {
      switch (className)
      {
         case "int" :
            ((int[])getTarget())[index] = (int) handler.getTarget();
            break;

         case "long" :
            ((long[])getTarget())[index] = (long) handler.getTarget();
            break;

         case "float" :
            ((float[])getTarget())[index] = (float) handler.getTarget();
            break;

         case "double" :
            ((double[])getTarget())[index] = (double) handler.getTarget();
            break;

         case "short" :
            ((short[])getTarget())[index] = (short) handler.getTarget();
            break;
            
         case "boolean" :
            ((boolean[])getTarget())[index] = (boolean) handler.getTarget();
            break;
           
         default :
            ((Object[])getTarget())[index] = handler.getTarget();
      }
      index++;
   }
}
