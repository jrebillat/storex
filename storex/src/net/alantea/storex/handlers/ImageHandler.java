package net.alantea.storex.handlers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

/**
 * The Class ImageHandler.
 */
public final class ImageHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, false);
  }};

   /**
    * Instantiates a new image handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @throws HandlerException the handler exception
    */
   public ImageHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
   }

   /**
    * Instantiates a new image handler.
    *
    * @param target the target
    */
   public ImageHandler(Object target)
   {
      super(target);
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#createElement(org.w3c.dom.Document, java.lang.Object)
    */
   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         BufferedImage img = (BufferedImage) target;
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         ImageIO.write(img, "png", baos);
         baos.flush();
         
         Writer.writeStartElement("bufferedimage");
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         Writer.writeCharacters(new String(Base64.getEncoder().encode(baos.toByteArray())));
         Writer.writeEndElement();
      }
      catch (StorexException | IOException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("ImageHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   /**
    * Handles read.
    *
    * @param type the type
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesRead(String type) throws HandlerException
   {
      return "bufferedimage".equals(type);
   }

   /**
    * Handles write.
    *
    * @param target the target
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesWrite(Object target) throws HandlerException
   {
      Class<?> contentType = target.getClass();
      return contentType.getName().equals(BufferedImage.class.getName());
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#onEnd()
    */
   @Override
   public void onEnd(String characters) throws HandlerException
   {
      try
      {
         byte[] bytes = Base64.getDecoder().decode(characters);
         BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));
         setTarget(image, getFather(), getField());
      }
      catch (IOException e)
      {
         throw new HandlerException("ImageHandler", HandlingMethod.READING, e);
      }
   }
}
