package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import net.alantea.liteprops.StringProperty;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

public final class StringHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, false);
  }};

   public StringHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      String content = null;
      if ((field != null) && (father != null))
      {
         field.setAccessible(true);
         try
         {
            if (field.getType().equals(String.class))
            {
               content = (String) field.get(father);
            }
            else if (field.getType().equals(StringProperty.class))
            {
               content = ((StringProperty) (field.get(father))).get();
            }
         }
         catch (IllegalArgumentException | IllegalAccessException e1)
         {
            e1.printStackTrace();
         }
         if (content == null)
         {
            content = "";
            try
            {
               field.set(father, content);
            }
            catch (IllegalArgumentException | IllegalAccessException e)
            {
               throw new HandlerException("ListHandler", HandlingMethod.CONSTRUCTOR, e);
            }
         }
      }
      else
      {
         content = "";
      }
      setTarget(content);
   }

   public StringHandler(Object target)
   {
      super(target);
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#onEnd()
    */
   @Override
   public void onEnd(String characters) throws HandlerException
   {
      if ((getField() != null) && (getField().getType().equals(StringProperty.class)))
      {
         setPropertyTarget(characters, getFather(), getField());
      }
      else
      {
         setTarget(characters, getFather(), getField());
      }
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         String content;
         
         if (target instanceof StringProperty)
         {
            content = ((StringProperty) target).get();
         }
         else
         {
            content = (String) target;
         }

         Writer.writeStartElement(Names.STRINGTAG);
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         if ((content != null) && (!content.isEmpty()))
         {
            Writer.writeCharacters(content);
         }
         Writer.writeEndElement();
      }
      catch (IllegalArgumentException e)
      {
         throw new HandlerException("StringHandler", HandlingMethod.WRITING, e);
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("StringHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return (String.class.equals(target.getClass())) || (StringProperty.class.equals(target.getClass()));
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.STRINGTAG.equals(type);
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}
