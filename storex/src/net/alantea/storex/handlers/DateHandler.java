package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class DateHandler.
 */
public final class DateHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CONTENTTAG, true);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, false);
  }};

   /**
    * Instantiates a new date handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @throws HandlerException the handler exception
    */
   public DateHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      String value = attributes.get(Names.CONTENTTAG);
      if (value != null)
      {
         Date target = new Date(Long.parseLong(value));
         setTarget(target, father, field);
      }
   }

   /**
    * Instantiates a new date handler.
    *
    * @param target the target
    */
   public DateHandler(Object target)
   {
      super(target);
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#createElement(org.w3c.dom.Document, java.lang.Object)
    */
   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      write(Names.DATETAG, name, Names.CONTENTTAG, Long.toString(((Date) target).getTime()));
   }

   /**
    * Handles write.
    *
    * @param target the target
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesWrite(Object target) throws HandlerException
   {
      return Date.class.equals(target.getClass());
   }

   /**
    * Handles read.
    *
    * @param type the type
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.DATETAG.equals(type);
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}
