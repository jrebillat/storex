package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.DoubleProperty;
import net.alantea.liteprops.FloatProperty;
import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.ListProperty;
import net.alantea.liteprops.LongProperty;
import net.alantea.liteprops.MapProperty;
import net.alantea.liteprops.Property;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

public final class ObjectPropertyHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CONTENTTAG, true);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, true);
  }};

   public ObjectPropertyHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);

      try
      {
         Object value = null;
         if (field != null)
         {
            value = field.get(father);
         }
         else
         {
            switch(type)
            {
               case Names.DOUBLEPROPERTYTAG :
                  value = new DoubleProperty(Double.parseDouble(attributes.get("content")));
                  break;

               case Names.INTEGERPROPERTYTAG :
                  value = new IntegerProperty(Integer.parseInt(attributes.get("content")));
                  break;

               case Names.LONGPROPERTYTAG :
                  value = new LongProperty(Long.parseLong(attributes.get("content")));
                  break;

               case Names.FLOATPROPERTYTAG :
                  value = new FloatProperty(Float.parseFloat(attributes.get("content")));
                  break;

               case Names.BOOLEANPROPERTYTAG :
                  value = new BooleanProperty();
                  ((BooleanProperty)value).set(Boolean.parseBoolean(attributes.get("content")));
                  break;
                  
            }
         }
         setTarget(value, father, field);
      }
      catch (IllegalArgumentException | IllegalAccessException e)
      {
         throw new HandlerException("ObjectPropertyHandler", HandlingMethod.READING, e);
      }
   }

   public ObjectPropertyHandler(Object target)
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         if (target instanceof ListProperty)
         {
            Writer.writeStartElement(Names.LISTPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            for (Object elt : (ListProperty<?>)target)
            {
               Writer.createElement(null, Names.CONTENTTAG, elt);
            }
         }
         else if (target instanceof MapProperty)
         {
            Writer.writeStartElement(Names.MAPPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            for (Object elt : ((MapProperty<?,?>)target).keySet())
            {
               MapData pair = new MapData();
               pair.setKey(elt);
               pair.setValue(((MapProperty<?,?>)target).get(elt));
               Writer.createElement(null, null, pair);
            }
         }
         else if (target instanceof IntegerProperty)
         {
            Writer.writeStartElement(Names.INTEGERPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            Writer.writeAttribute(Names.CONTENTTAG, (String) ((IntegerProperty) target).get().toString());
         }
         else if (target instanceof DoubleProperty)
         {
            Writer.writeStartElement(Names.DOUBLEPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            Writer.writeAttribute(Names.CONTENTTAG, (String) ((DoubleProperty) target).get().toString());
         }
         else if (target instanceof FloatProperty)
         {
            Writer.writeStartElement(Names.FLOATPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            Writer.writeAttribute(Names.CONTENTTAG, (String) ((IntegerProperty) target).get().toString());
         }
         else if (target instanceof LongProperty)
         {
            Writer.writeStartElement(Names.LONGPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            Writer.writeAttribute(Names.CONTENTTAG, (String) ((DoubleProperty) target).get().toString());
         }
         else if (target instanceof BooleanProperty)
         {
            Writer.writeStartElement(Names.BOOLEANPROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            Writer.writeAttribute(Names.CONTENTTAG, (String) ((DoubleProperty) target).get().toString());
         }
         else
         {
            Writer.writeStartElement(Names.PROPERTYTAG);
            if (name != null)
            {
               Writer.writeAttribute(Names.FIELDNAME, name);
            }
            Writer.createElement(null, Names.CONTENTTAG, ((Property<?>)target).get());
         }
         Writer.writeEndElement();
      }
      catch (IllegalArgumentException e)
      {
         throw new HandlerException("ObjectPropertyHandler", HandlingMethod.WRITING, e);
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("ObjectPropertyHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      if (IntegerProperty.class.isAssignableFrom(target.getClass()))
      {
         return false;
      }
      return (Property.class.isAssignableFrom(target.getClass()));
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return (Names.PROPERTYTAG.equals(type))
            || (Names.LISTPROPERTYTAG.equals(type))
            || (Names.MAPPROPERTYTAG.equals(type))
            || (Names.BOOLEANPROPERTYTAG.equals(type))
            || (Names.DOUBLEPROPERTYTAG.equals(type))
            || (Names.FLOATPROPERTYTAG.equals(type))
            || (Names.INTEGERPROPERTYTAG.equals(type))
            || (Names.LONGPROPERTYTAG.equals(type));
   }
   
   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#insert(net.alantea.storex.handlers.Handler)
    */
   @SuppressWarnings("unchecked")
   @Override
   public void insert(Handler handler) throws HandlerException
   {
      Object target = getTarget();

      if (target instanceof ListProperty)
      {
         ((ListProperty<Object>)target).add(handler.getTarget());
      }
      else if (target instanceof MapProperty)
      {
         MapData pair = (MapData) handler.getTarget();
         ((Map<Object, Object>)target).put(pair.getKey(), pair.getValue());
      }
      else
      {
         ((Property<Object>)target).set(handler.getTarget());
      }
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
   
   private static class MapData
   {
      private Object key;
      private Object value;
      
      MapData()
      {
      }

      public Object getKey()
      {
         return key;
      }

      public void setKey(Object key)
      {
         this.key = key;
      }

      public Object getValue()
      {
         return value;
      }

      public void setValue(Object value)
      {
         this.value = value;
      }
      
   }
}
