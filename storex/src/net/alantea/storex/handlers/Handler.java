package net.alantea.storex.handlers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.DoubleProperty;
import net.alantea.liteprops.FloatProperty;
import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.LongProperty;
import net.alantea.liteprops.Property;
import net.alantea.liteprops.StringProperty;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

/**
 * The Class Handler.
 */
public class Handler
{
   
   /** The ordered handlers. */
   private static Stack<Class<?>> orderedHandlers = new Stack<>();
   
   /** The mapped handlers. */
   private static Map<Class<?>, Invoker> mappedHandlers = new HashMap<>();
   
   /** The type. */
   private String type;
   
   /** The field. */
   private Field field;
   
   /** The target. */
   private Object target;
   
   /** The father. */
   private Object father;
   
   /** The attributes. */
   private Map<String, String> attributes;
   
   /** Characters buffer. */
   private StringBuffer buffer = new StringBuffer();

   static
   {
      // CAUTION : reverse ordering !
      Class<?>[] handlerClasses = {
         ObjectHandler.class,
         ArrayHandler.class,
         MapHandler.class,
         ListHandler.class,
         EnumHandler.class,
         ImageHandler.class,
         DateHandler.class,
         ObjectPropertyHandler.class,
         StringHandler.class,
         PrimitiveHandler.class,
         ByteArrayHandler.class,
      };
      for (Class<?> cl : handlerClasses)
      {
            try
            {
               @SuppressWarnings("unchecked")
               Class<? extends Handler> handler = (Class<? extends Handler>) cl;
               prependHandler(handler);
            }
            catch (StorexException e)
            {
               // Should never get here...
            }
      }
   }
   
   /**
    * Instantiates a new handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    */
   public Handler(Object father, Field field, String type, Map<String, String> attributes)
   {
      this.type = type;
      this.field = field;
      if (field != null)
      {
         field.setAccessible(true);
      }
      this.father = father;
      this.attributes = attributes;
   }

   /**
    * Instantiates a new handler.
    *
    * @param target the target
    */
   public Handler(Object target)
   {
      this.target = target;
   }
   
   /**
    * Prepend handler to list.
    *
    * @param cl the class
    * @throws StorexException the storex exception
    */
   public static final void prependHandler(Class<? extends Handler> cl) throws StorexException
   {
      try
      {
         Method handlesRead = cl.getDeclaredMethod("handlesRead", String.class);
         Method handlesWrite = cl.getDeclaredMethod("handlesWrite", Object.class);
         Method handledAttributes = cl.getDeclaredMethod("getHandledAttributes");
         Constructor<?> readConstructor = cl.getDeclaredConstructor(Object.class, Field.class, String.class, Map.class);
         Constructor<?> writeConstructor = cl.getDeclaredConstructor(Object.class);
         if ((Modifier.isStatic(handlesRead.getModifiers())) && (Modifier.isStatic(handlesWrite.getModifiers())))
         {
            mappedHandlers.put(cl, new Invoker(readConstructor, writeConstructor, handlesRead, handlesWrite, handledAttributes));
            // add at start
            orderedHandlers.add(0, cl);
         }
      }
      catch (NoSuchMethodException | SecurityException e)
      {
         throw new StorexException(e);
      }

   }

   /**
    * Handles read.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesRead(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      return false;
   }
   
   /**
    * Handles write.
    *
    * @param target the target
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesWrite(Object target) throws HandlerException
   {
      return false;
   }
   
   /**
    * Get invoker.
    *
    * @param type the type
    * @param attributes the attributes
    * @return the invoker
    * @throws HandlerException the handler exception
    */
   public static final Invoker getInvoker(String type, Map<String, String> attributes) throws HandlerException
   {
      for (Class<?> handlerClass : orderedHandlers)
      {
         try
         {
            Invoker invoker = mappedHandlers.get(handlerClass);
            if ((boolean) invoker.getHandlesRead().invoke(null, type))
            {
               @SuppressWarnings("unchecked")
               Map<String, Boolean> attrMap = (Map<String, Boolean>) invoker.getHandledAttributes().invoke(null);
               boolean moreAttributes = attrMap.get(Names.ALLOWOTHERTAGS);
               for (String key : attributes.keySet())
               {
                  if ((attrMap.get(key) == null) && (!moreAttributes))
                  {
                     throw new HandlerException("Handler", HandlingMethod.INVOKER, key + "not allowed");
                  }
               }
               for (String key : attrMap.keySet())
               {
                  if ((!Names.ALLOWOTHERTAGS.equals(key)) 
                        && (!Names.MAYHAVECHILDREN.equals(key))
                        && (!Names.CONTENTTAG.equals(key)))
                  {
                     if ((attributes.get(key) == null) && (attrMap.get(key)))
                     {
                        throw new HandlerException("Handler", HandlingMethod.INVOKER, key + " required");
                     }
                  }
               }
               return invoker;
            }
         }
         catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
         {
            throw new HandlerException("Handler", HandlingMethod.INVOKER, e);
         }
      }
      throw new HandlerException("Handler", HandlingMethod.INVOKER, new NullPointerException());
   }
   
   /**
    * Load handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @return the handler
    * @throws HandlerException the handler exception
    */
   public static final Handler loadHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      Invoker invoker = getInvoker(type, attributes);
      try
      {
         if (invoker != null)
         {
            return (Handler) invoker.getReadConstructor().newInstance(father, field, type, attributes);
         }
      }
      catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e)
      {
         throw new HandlerException("Handler", HandlingMethod.HANDLER, e);
      }
      throw new HandlerException("Handler", HandlingMethod.HANDLER, new NullPointerException());
   }

   /**
    * Load handler.
    *
    * @param target the target
    * @return the handler
    * @throws HandlerException the handler exception
    */
   public static final Handler loadHandler(Object target) throws HandlerException
   {
      for (Class<?> handlerClass : orderedHandlers)
      {
         try
         {
            if ((boolean) mappedHandlers.get(handlerClass).getHandlesWrite().invoke(null, target))
            {
               return (Handler) mappedHandlers.get(handlerClass).getWriteConstructor().newInstance(target);
            }
         }
         catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e)
         {
            throw new HandlerException("Handler", HandlingMethod.HANDLER, e);
         }
      }
      throw new HandlerException("Handler", HandlingMethod.HANDLER, new NullPointerException());
   }
   
   
   /**
    * Gets the type.
    *
    * @return the type
    */
   public final String getType()
   {
      return type;
   }

   /**
    * Gets the field.
    *
    * @return the field
    */
   public final Field getField()
   {
      return field;
   }

   /**
    * Gets the father.
    *
    * @return the father
    */
   public final Object getFather()
   {
      return father;
   }

   /**
    * Gets the attributes.
    *
    * @return the attributes
    */
   public final Map<String, String> getAttributes()
   {
      return attributes;
   }

   /**
    * Gets the target.
    *
    * @return the target
    */
   public final Object getTarget()
   {
      return target;
   }

   /**
    * Sets the target.
    *
    * @param target the new target
    */
   public final void setTarget(Object target)
   {
      this.target = target;
   }

   /**
    * Sets the target.
    *
    * @param target the new target
    * @param father the father
    * @param field the field
    * @throws HandlerException the handler exception
    */
   public final void setTarget(Object target, Object father, Field field) throws HandlerException
   {
      if (target == null)
      {
         return;
      }
      setTarget(target);
      if ((field != null) && (field.getType().isAssignableFrom(target.getClass())))
      {
         if (target != null)
         {
            try
            {
               field.set(father, target);
            }
            catch (IllegalArgumentException | IllegalAccessException e)
            {
               throw new HandlerException(getClass().getSimpleName(), HandlingMethod.READING, e);
            }
         }
      }
   }

   /**
    * Sets the target in property field.
    *
    * @param target the new target
    * @param father the father
    * @param field the field
    * @throws HandlerException the handler exception
    */
   @SuppressWarnings("unchecked")
   public final void setPropertyTarget(Object target, Object father, Field field) throws HandlerException
   {
      if (target == null)
      {
         return;
      }
      if ((field != null) && (Property.class.isAssignableFrom(field.getType())))
      {
         setTarget(target);
         if (target != null)
         {
            try
            {
               ((Property<Object>) field.get(father)).set(target);
            }
            catch (IllegalArgumentException | IllegalAccessException e)
            {
               throw new HandlerException(getClass().getSimpleName(), HandlingMethod.READING, e);
            }
         }
      }
      else
      {
         switch (target.getClass().getSimpleName())
         {
            case "Integer" :
               setTarget(new IntegerProperty((Integer) target));
               break;
               
            case "Long" :
               setTarget(new LongProperty((Long) target));
               break;
               
            case "Float" :
               setTarget(new FloatProperty((Float) target));
               break;
               
            case "Double" :
               setTarget(new DoubleProperty((Double) target));
               break;
               
            case "Boolean" :
               setTarget(new BooleanProperty((Boolean) target));
               break;
               
            case "String" :
               setTarget(new StringProperty((String) target));
               break;
               
            default :
               setTarget(new Property<Object>(target));
               break;
         }
      }
   }

   /**
    * Creates the element.
    *
    * @param target the target
    * @param name the name
    * @throws HandlerException the handler exception
    */
   public void createElement(Object target, String name) throws HandlerException
   {
      return;
   }
   
   /**
    * On end.
    *
    * @param characters the characters
    * @throws HandlerException the handler exception
    */
   public void onEnd(String characters) throws HandlerException
   {
      // default : empty
   }
   
   /**
    * Insert.
    *
    * @param handler the handler
    * @throws HandlerException the handler exception
    */
   public void insert(Handler handler) throws HandlerException
   {
      // default : empty
   }
   
   /**
    * Write.
    *
    * @param tagName the tag name
    * @param fieldName the field name
    * @param args the attributes as ( key, value)
    * @throws HandlerException the handler exception
    */
   protected void write(String tagName, String fieldName, String... args) throws HandlerException
   {
      if ((args.length % 2) == 1)
      {
         write(tagName, fieldName, createMap(args), args[args.length -1]);
      }
      else
      {
         write(tagName, fieldName, createMap(args), args[args.length -1]);
      }
   }
   
   /**
    * Write.
    *
    * @param tagName the tag name
    * @param fieldName the field name
    * @param attributesMap the attributes map
    * @param characters the characters
    * @throws HandlerException the handler exception
    */
   protected void write(String tagName, String fieldName, Map<String, String> attributesMap, String characters) throws HandlerException
   {
      write(tagName, fieldName, null, attributesMap, null, characters);
   }
   
   /**
    * Write.
    *
    * @param tagName the tag name
    * @param fieldName the field name
    * @param target the target
    * @param attributesMap the attributes map
    * @param fieldsList the fields list
    * @param characters the characters
    * @throws HandlerException the handler exception
    */
   protected void write(String tagName, String fieldName, Object target, Map<String, String> attributesMap, List<Field> fieldsList, String characters) throws HandlerException
   {
      startWriting(tagName, fieldName, attributesMap);
      if (fieldsList != null)
      {
         for (Field field : fieldsList)
         {
            writeField(target, field);
         }
      }
      try
      {
         Writer.writeCharacters(characters);
      }
      catch (StorexException e)
      {
         throw new HandlerException(getClass().getSimpleName(), HandlingMethod.WRITING, e);
      }
      endWriting();
   }

   /**
    * Start writing.
    *
    * @param tagName the tag name
    * @param fieldName the field name
    * @param attributesMap the attributes map
    * @throws HandlerException the handler exception
    */
   protected void startWriting(String tagName, String fieldName, Map<String, String> attributesMap) throws HandlerException
   {
      try
      {
         Writer.writeStartElement(tagName);
         if (fieldName != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, fieldName);
         }
         if (attributesMap != null)
         {
            for (String key : attributesMap.keySet())
            {
               Writer.writeAttribute(key, attributesMap.get(key));
            }
         }
      }
      catch (StorexException e)
      {
         throw new HandlerException(getClass().getSimpleName(), HandlingMethod.WRITING, e);
      }
   }

   /**
    * Write field.
    *
    * @param target the target
    * @param field the field
    * @throws HandlerException the handler exception
    */
   protected final void writeField(Object target, Field field) throws HandlerException
   {
      try
      {
         field.setAccessible(true);
         Writer.createElement(field, field.getName(), field.get(target));
      }
      catch (IllegalArgumentException | IllegalAccessException e)
      {
         throw new HandlerException(getClass().getSimpleName(), HandlingMethod.WRITING, e);
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException(getClass().getSimpleName(), HandlingMethod.READING, e1);
         }
      }
   }

   /**
    * End writing.
    *
    * @throws HandlerException the handler exception
    */
   protected final void endWriting() throws HandlerException
   {
      try
      {
         Writer.writeEndElement();
      }
      catch (StorexException e)
      {
         throw new HandlerException(getClass().getSimpleName(), HandlingMethod.WRITING, e);
      }
   }
   
   protected Map<String, String> createMap(String... args)
   {
      Map<String, String> ret = new HashMap<>();
      for (int i = 0; i < args.length -1; i += 2)
      {
         ret.put(args[i], args[i + 1]);
      }
      return ret;
   }
   
   /**
    * Adds the characters.
    *
    * @param ch the ch
    * @param start the start
    * @param length the length
    */
   public void addCharacters(char ch[],int start,int length)
   {
      buffer.append(ch, start, length);
   }

   /**
    * Gets the buffer content.
    *
    * @return the buffer content
    */
   public String getBufferContent()
   {
      return buffer.toString();
   }
   
   /**
    * The Class Invoker.
    */
   static private class Invoker
   {
      
      /** The read constructor. */
      private Constructor<?> readConstructor;
      
      /** The write constructor. */
      private Constructor<?> writeConstructor;
      
      /** The handles read. */
      private Method handlesRead;
      
      /** The handles write. */
      private Method handlesWrite;
      
      /** The attributes map. */
      private Method handledAttributes;
      
      /**
       * Instantiates a new invoker.
       *
       * @param readConstructor the read constructor
       * @param writeConstructor the write constructor
       * @param handlesRead the handles read
       * @param handlesWrite the handles write
       */
      public Invoker(Constructor<?> readConstructor, Constructor<?> writeConstructor,
            Method handlesRead, Method handlesWrite,
            Method handledAttributes)
      {
         this.readConstructor = readConstructor;
         this.writeConstructor = writeConstructor;
         this.handlesRead = handlesRead;
         this.handlesWrite = handlesWrite;
         this.handledAttributes = handledAttributes;
         handlesRead.setAccessible(true);
         handlesWrite.setAccessible(true);
         readConstructor.setAccessible(true);
         writeConstructor.setAccessible(true);
         handledAttributes.setAccessible(true);
      }

      /**
       * Gets the read constructor.
       *
       * @return the read constructor
       */
      public Constructor<?> getReadConstructor()
      {
         return readConstructor;
      }

      /**
       * Gets the write constructor.
       *
       * @return the write constructor
       */
      public Constructor<?> getWriteConstructor()
      {
         return writeConstructor;
      }

      /**
       * Gets the handles read.
       *
       * @return the handles read
       */
      public Method getHandlesRead()
      {
         return handlesRead;
      }

      /**
       * Gets the handles write.
       *
       * @return the handles write
       */
      public Method getHandlesWrite()
      {
         return handlesWrite;
      }

      /**
       * Gets the handles attributes.
       *
       * @return the handles attributes
       */
      public Method getHandledAttributes()
      {
         return handledAttributes;
      }
   }
}
