package net.alantea.storex.handlers;

/**
 * The Class StorexNames.
 */
public class Names
{
   
   /** The Constant FIELDNAME. */
   // Elements tags
   public static final String FIELDNAME = "field";
   
   /** The Constant OBJECTTAG. */
   public static final String OBJECTTAG = "object";
   
   /** The Constant ARRAYTAG. */
   public static final String ARRAYTAG = "array";
   
   /** The Constant BYTEARRAYTAG. */
   public static final String BYTEARRAYTAG = "bytearray";
   
   /** The Constant LISTTAG. */
   public static final String LISTTAG = "list";
   
   /** The Constant MAPTAG. */
   public static final String MAPTAG = "map";
   
   /** The Constant ENUMTAG. */
   public static final String ENUMTAG = "enum";

   /** The Constant PROPERTYTAG. */
   public static final String PROPERTYTAG = "property";

   /** The Constant PROPERTYTAG. */
   public static final String BOOLEANPROPERTYTAG = "booleanproperty";
   
   /** The Constant DOUBLEPROPERTYTAG. */
   public static final String DOUBLEPROPERTYTAG = "doubleproperty";
   
   /** The Constant FLOATPROPERTYTAG. */
   public static final String FLOATPROPERTYTAG = "floatproperty";
   
   /** The Constant INTEGERPROPERTYTAG. */
   public static final String INTEGERPROPERTYTAG = "integerproperty";
   
   /** The Constant LONGPROPERTYTAG. */
   public static final String LONGPROPERTYTAG = "longproperty";

   /** The Constant LISTPROPERTYTAG. */
   public static final String LISTPROPERTYTAG = "listproperty";
   
   /** The Constant MAPPROPERTYTAG. */
   public static final String MAPPROPERTYTAG = "mapproperty";
   
   /** The Constant STRINGTAG. */
   public static final String STRINGTAG = "string";
   
   /** The Constant DATETAG. */
   public static final String DATETAG = "date";

   // Primitive tags
   /** The Constant FLOATTAG. */
   public static final String FLOATTAG = "float";
   
   /** The Constant INTEGERTAG. */
   public static final String INTEGERTAG = "integer";
   
   /** The Constant LONGTAG. */
   public static final String LONGTAG = "long";
   
   /** The Constant DOUBLETAG. */
   public static final String DOUBLETAG = "double";
   
   /** The Constant BOOLEANTAG. */
   public static final String BOOLEANTAG = "boolean";
   
   /** The Constant SHORTTAG. */
   public static final String SHORTTAG = "short";
   
   /** The Constant BYTETAG. */
   public static final String BYTETAG = "byte";

   // Attribute tags
   /** The Constant CONTENTTAG. */
   public static final String CONTENTTAG = "content";
   
   /** The Constant SIZETAG. */
   public static final String SIZETAG = "size";
   
   /** The Constant CLASSTAG. */
   public static final String CLASSTAG = "class";
   
   /** The Constant ALLOWOTHERTAGS. */
   public static final String ALLOWOTHERTAGS = "allowOtherTags";
   
   /** The Constant MAYHAVECHILDREN. */
   public static final String MAYHAVECHILDREN = "mayHaveChildren";
}
