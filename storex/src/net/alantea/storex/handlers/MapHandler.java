package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

public final class MapHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, true);
  }};

   @SuppressWarnings("unchecked")
   public MapHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      if (Names.MAPTAG.equals(type))
      {
         Map<Object, Object> map = null;
         if ((field != null) && (father != null))
         {
            field.setAccessible(true);
            try
            {
               map = (Map<Object, Object>) field.get(father);
            }
            catch (IllegalArgumentException | IllegalAccessException e1)
            {
               throw new HandlerException("MapHandler", HandlingMethod.READING, e1);
            }
            if (map == null)
            {
               map = new HashMap<>();
               try
               {
                  field.set(father, map);
               }
               catch (IllegalArgumentException | IllegalAccessException e)
               {
                  throw new HandlerException("MapHandler", HandlingMethod.READING, e);
               }
            }
         }
         else
         {
            map = new HashMap<>();
         }
         setTarget(map);
      }
   }

   public MapHandler(Object target)
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         Writer.writeStartElement(Names.MAPTAG);
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         @SuppressWarnings("unchecked")
         Map<Object, Object> map = (Map<Object, Object>) target;
         for (Object object : map.keySet())
         {
               MapData pair = new MapData();
               pair.setKey(object);
               pair.setValue(map.get(object));
               Writer.createElement(null, null, pair);
         }
         Writer.writeEndElement();
      }
      catch (IllegalArgumentException e)
      {
         throw new HandlerException("MapHandler", HandlingMethod.WRITING, e);
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("MapHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.MAPTAG.equals(type);
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return target instanceof Map;
   }
   
   @Override
   @SuppressWarnings("unchecked")
   public void insert(Handler handler) throws HandlerException
   {
      MapData pair = (MapData) handler.getTarget();
      ((Map<Object, Object>)getTarget()).put(pair.getKey(), pair.getValue());
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
   
   private static class MapData
   {
      private Object key;
      private Object value;
      
      MapData()
      {
      }

      public Object getKey()
      {
         return key;
      }

      public void setKey(Object key)
      {
         this.key = key;
      }

      public Object getValue()
      {
         return value;
      }

      public void setValue(Object value)
      {
         this.value = value;
      }
      
   }
}
