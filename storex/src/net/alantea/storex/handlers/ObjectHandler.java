package net.alantea.storex.handlers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.xml.sax.SAXException;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.DoubleProperty;
import net.alantea.liteprops.FloatProperty;
import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.LongProperty;
import net.alantea.liteprops.Property;
import net.alantea.liteprops.StringProperty;
import net.alantea.storex.StorexClassLoader;
import net.alantea.storex.StorexException;
import net.alantea.storex.Writer;
import net.alantea.storex.handlers.HandlerException.HandlingMethod;

/**
 * The Class ObjectHandler.
 */
public final class ObjectHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CLASSTAG, true);
      put(Names.ALLOWOTHERTAGS, true);
      put(Names.MAYHAVECHILDREN, true);
  }};
   
   /**
    * The Enum Parameters.
    */
   private static enum Parameters
   {
      
      /** The bad. */
      BAD,
      
      /** The none. */
      NONE,
      
      /** The father. */
      FATHER,
      
      /** The map. */
      MAP,
      
      /** The fathermap. */
      FATHERMAP,
      
      /** The fatherattributes. */
      FATHERATTRIBUTES
   }
   
   /** The constructors map. */
   private static Map<Class<?>, Map<Class<?>, Pair<Parameters,Constructor<?>>>> constructorsMap = new HashMap<>();

   /**
    * Instantiates a new object handler.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @throws HandlerException the handler exception
    */
   public ObjectHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      createTarget(father, field, type, attributes);
      Object content = getTarget();
      if ((field != null) && (content != null) && (field.getType().isAssignableFrom(content.getClass())))
      {
         try
         {
            field.setAccessible(true);
            field.set(father, content);
         }
         catch (IllegalArgumentException | IllegalAccessException e)
         {
            throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
         }
      }
   }

   /**
    * Instantiates a new object handler.
    *
    * @param target the target
    */
   public ObjectHandler(Object target)
   {
      super(target);
   }

   /* (non-Javadoc)
    * @see net.alantea.storex.handlers.Handler#createElement(org.w3c.dom.Document, java.lang.Object)
    */
   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      try
      {
         Writer.writeStartElement(Names.OBJECTTAG);
         if (name != null)
         {
            Writer.writeAttribute(Names.FIELDNAME, name);
         }
         Writer.writeAttribute(Names.CLASSTAG, target.getClass().getName());
         fillFields(target);
         Writer.writeEndElement();
      }
      catch (IllegalArgumentException | XMLStreamException e)
      {
         throw new HandlerException("ObjectHandler", HandlingMethod.WRITING, e);
      }
      catch (StorexException e1)
      {
         if (e1 instanceof HandlerException)
         {
            throw (HandlerException)e1;
         }
         else
         {
            throw new HandlerException("ObjectHandler", HandlingMethod.WRITING, e1);
         }
      }
   }

   /**
    * Handles write.
    *
    * @param target the target
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesWrite(Object target) throws HandlerException
   {
      // The last in line !
      return true;
   }

   /**
    * Handles read.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @return true, if successful
    * @throws HandlerException the handler exception
    */
   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.OBJECTTAG.equals(type);
   }

   /**
    * Fill fields.
    *
    * @param target the target
    * @throws SAXException the SAX exception
    * @throws XMLStreamException the XML stream exception
    * @throws HandlerException the handler exception
    */
   private void fillFields(Object target) throws XMLStreamException, HandlerException
   {
      fillAttributesFields(target, target.getClass());
      fillChildrenFields(target, target.getClass());
   }

   /**
    * Fill fields.
    *
    * @param target the target
    * @param cl the cl
    * @throws SAXException the SAX exception
    * @throws XMLStreamException the XML stream exception
    * @throws HandlerException the handler exception
    */
private void fillAttributesFields(Object target, Class<?> cl) throws XMLStreamException, HandlerException
   {
      for (Field field : cl.getDeclaredFields())
      {
         if ((Modifier.isTransient(field.getModifiers()))
               || (Modifier.isStatic(field.getModifiers())) 
               || (Modifier.isFinal(field.getModifiers())))
         {
            continue;
         }
         field.setAccessible(true);

         if ((field.getType().isPrimitive()) 
               || (IntegerProperty.class.isAssignableFrom(field.getType()))
               || (LongProperty.class.isAssignableFrom(field.getType()))
               || (FloatProperty.class.isAssignableFrom(field.getType()))
               || (BooleanProperty.class.isAssignableFrom(field.getType()))
               || (DoubleProperty.class.isAssignableFrom(field.getType())))
         {
            String name = field.getName();
            try
            {
               field.setAccessible(true);
               if (Property.class.isAssignableFrom(field.getType()))
               {
                  @SuppressWarnings("unchecked")
                  Property<Object> property = (Property<Object>)(field.get(target));
                  if (property != null)
                  {
                     Object val = property.get();
                     if (val != null)
                     {
                        Writer.writeAttribute(name, val.toString());
                     }
                  }
               }
               else if (field.get(target) != null)
               {
                  Writer.writeAttribute(name, field.get(target).toString());
               }
            }
            catch (IllegalArgumentException | IllegalAccessException | StorexException e)
            {
               throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
            }
         }
         else
         {
         }
      }
      if (!Object.class.equals(cl))
      {
         fillAttributesFields(target, cl.getSuperclass());
      }
   }

   /**
    * Fill fields.
    *
    * @param target the target
    * @param cl the cl
    * @throws SAXException the SAX exception
    * @throws XMLStreamException the XML stream exception
    * @throws HandlerException the handler exception
    */
private void fillChildrenFields(Object target, Class<?> cl) throws XMLStreamException, HandlerException
   {
      for (Field field : cl.getDeclaredFields())
      {
         if ((Modifier.isTransient(field.getModifiers()))
               || (Modifier.isStatic(field.getModifiers())) 
               || (Modifier.isFinal(field.getModifiers())))
         {
            continue;
         }
         field.setAccessible(true);

         if ((field.getType().isPrimitive()) 
               || (IntegerProperty.class.isAssignableFrom(field.getType()))
               || (LongProperty.class.isAssignableFrom(field.getType()))
               || (FloatProperty.class.isAssignableFrom(field.getType()))
               || (BooleanProperty.class.isAssignableFrom(field.getType()))
               || (DoubleProperty.class.isAssignableFrom(field.getType())))
         {
         }
         else
         {
            try
            {
               Writer.createElement(field, field.getName(), field.get(target));
            }
            catch (IllegalArgumentException | IllegalAccessException e)
            {
               throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
            }
            catch (StorexException e1)
            {
               if (e1 instanceof HandlerException)
               {
                  throw (HandlerException)e1;
               }
               else
               {
                  throw new HandlerException("ObjectHandler", HandlingMethod.READING, e1);
               }
            }
         }
      }
      if (!Object.class.equals(cl))
      {
         fillChildrenFields(target, cl.getSuperclass());
      }
   }
   
   /**
    * Creates the target.
    *
    * @param father the father
    * @param field the field
    * @param type the type
    * @param attributes the attributes
    * @throws HandlerException the handler exception
    */
   private void createTarget(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      try
      {
         Class<?> myClass = StorexClassLoader.getInstance().loadClass(attributes.get(Names.CLASSTAG));
         Pair<Parameters, Constructor<?>> consInfo = chooseConstructor(myClass, (father == null) ? null : father.getClass());
         Object me = createInstance(consInfo, father, attributes);
         readTarget(father, me, attributes, myClass);
         setTarget(me);
      }
      catch (ClassNotFoundException | SecurityException | IllegalArgumentException e)
      {
         throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
      }
   }
   
   /**
    * Choose constructor.
    *
    * @param myClass the my class
    * @param fatherClass the father class
    * @return the pair
    */
   private Pair<Parameters, Constructor<?>> chooseConstructor(Class<?> myClass, Class<?> fatherClass)
   {
      // search constructor
      Map<Class<?>, Pair<Parameters, Constructor<?>>> map = constructorsMap.get(fatherClass);
      if (map == null)
      {
         map = new HashMap<>();
         constructorsMap.put(fatherClass, map);
      }
      Pair<Parameters, Constructor<?>> ret =  map.get(myClass);
      if (ret == null)
      {
         ret = buildInfo(myClass, fatherClass, map);
      }  
      return ret;
   }

   /**
    * Builds the info.
    *
    * @param myClass the my class
    * @param fatherClass the father class
    * @param map the map
    * @return the pair
    */
   private Pair<Parameters, Constructor<?>> buildInfo(Class<?> myClass, Class<?> fatherClass, Map<Class<?>, Pair<Parameters, Constructor<?>>> map)
   {
      Constructor<?> foundConstructor = null;
      Constructor<?> emptyCons = null;
      Constructor<?> mapCons = null;
      Constructor<?> fatherCons = null;
      Constructor<?>[] cons = myClass.getDeclaredConstructors();
      Parameters foundParameters = Parameters.BAD;
      for (Constructor<?> con : cons)
      {
         Class<?>[] parmsTypes = con.getParameterTypes();
         switch(con.getParameterCount())
         {
            case 0 :
               foundParameters = Parameters.NONE;
               emptyCons = con;
               break;

            case 1 :
               if ((parmsTypes[0].isAssignableFrom(Map.class)))
               {
                  foundParameters = Parameters.MAP;
                  mapCons = con;
               }
               else if ((fatherClass != null)
                     && (parmsTypes[0].isAssignableFrom(fatherClass)))
               {
                  foundParameters = Parameters.FATHER;
                  fatherCons = con;
               }
               break;

            case 2 :
               if ((fatherClass != null) && (parmsTypes[0].isAssignableFrom(fatherClass)))
               {
                  if ((parmsTypes[1].isAssignableFrom(Map.class)))
                  {
                     foundParameters = Parameters.FATHERMAP;
                     foundConstructor = con;
                  }
               }
         }
      }
      if (foundConstructor == null)
      {
         if (fatherCons != null)
         {
            foundConstructor = fatherCons;
         }
         else if (mapCons != null)
         {
            foundConstructor = mapCons;
         }
         else
         {
            foundConstructor = emptyCons;

         }
      }

      if (foundConstructor != null)
      {
         foundConstructor.setAccessible(true);
      }
      Pair<Parameters, Constructor<?>> ret = new Pair<>(foundParameters, foundConstructor);
      map.put(myClass, ret);
      return ret;
   }

   /**
    * Creates the instance.
    *
    * @param consInfo the cons info
    * @param father the father
    * @param attributes the attributes
    * @return the object
    * @throws HandlerException the handler exception
    */
   private Object createInstance(Pair<Parameters, Constructor<?>> consInfo, Object father, Map<String, String> attributes) throws HandlerException
   {
      Object ret = null;
      try
      {
         switch(consInfo.getKey())
         {
            case NONE :
               ret = consInfo.getValue().newInstance();
               break;

            case FATHER :
               ret = consInfo.getValue().newInstance(father);
               break;

            case MAP :
               Map<String, String> attrsMap = new HashMap<>();
               for (String key : attributes.keySet())
               {
                  attrsMap.put(key, attributes.get(key));
               }
               ret = consInfo.getValue().newInstance(attrsMap);
               break;

            case FATHERATTRIBUTES :
               ret = consInfo.getValue().newInstance(father, attributes);
               break;

            case FATHERMAP :
               Map<String, String> attrsMap1 = new HashMap<>();
               for (String key : attributes.keySet())
               {
                  attrsMap1.put(key, attributes.get(key));
               }
               ret = consInfo.getValue().newInstance(father, attrsMap1);
               break;

            case BAD :
               // Too bad...
         }
      }
      catch (InstantiationException | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException e)
      {
         throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
      }
      return ret;
   }

   /**
    * Read target.
    *
    * @param father the father
    * @param me the me
    * @param attributes the attributes
    * @param cl the cl
    * @throws HandlerException the handler exception
    */
private void readTarget(Object father, Object me, Map<String, String> attributes, Class<?> cl) throws HandlerException
   {
      if (me == null)
      {
         return;
      }
      for (Field field : cl.getDeclaredFields())
      {
         if ((Modifier.isTransient(field.getModifiers()))
               || (Modifier.isStatic(field.getModifiers())) 
               || (Modifier.isFinal(field.getModifiers())))
         {
            continue;
         }
         field.setAccessible(true);

         if ((field.getType().isPrimitive()) 
               || (String.class.equals(field.getType()))
               || ((Property.class.isAssignableFrom(field.getType()))))
         {
            String value = attributes.get(field.getName());

            if (value != null)
            {
               try
               {
                  field.setAccessible(true);
                  Object v = null;
                  if (Property.class.isAssignableFrom(field.getType()))
                  {
                     v = ((Property<?>)field.get(me));
                     Class<?> pt = v.getClass();
                     if (StringProperty.class.isAssignableFrom(pt))
                     {
                        ((StringProperty) v).set(value);
                     }
                     else if (IntegerProperty.class.isAssignableFrom(pt))
                     {
                        ((IntegerProperty) v).set(Integer.parseInt(value));
                     }
                     else if (DoubleProperty.class.isAssignableFrom(pt))
                     {
                        ((DoubleProperty) v).set(Double.parseDouble(value));
                     }
                     else if (FloatProperty.class.isAssignableFrom(pt))
                     {
                        v = Float.parseFloat(value);
                     }
                     else if (LongProperty.class.isAssignableFrom(pt))
                     {
                        ((LongProperty) v).set(Long.parseLong(value));
                     }
                     else if (BooleanProperty.class.isAssignableFrom(pt))
                     {
                        ((BooleanProperty) v).set(Boolean.parseBoolean(value));
                     }
                  }
                  else
                  {
                     v = getAttributeValue(field.getType().getName(), value);
                  }
                  if (v != null)
                  {
                     if ((! field.getType().equals(Date.class)) && (field.getType().isAssignableFrom(v.getClass())))
                     {
                        field.set(me, v);
                     }
                     else if (field.getType().getName().equals("int"))
                     {
                        field.set(me, (int)v);
                     }
                     else if (field.getType().getName().equals("double"))
                     {
                        field.set(me, (double)v);
                     }
                     else if (field.getType().getName().equals("long"))
                     {
                        field.set(me, (long)v);
                     }
                     else if (field.getType().getName().equals("float"))
                     {
                        field.set(me, (float)v);
                     }
                     else if (field.getType().getName().equals("boolean"))
                     {
                        field.set(me, (boolean)v);
                     }
                     else if (field.getType().getName().equals("short"))
                     {
                        field.set(me, (short)v);
                     }
                     else if (field.getType().getName().equals("byte"))
                     {
                        field.set(me, (byte)v);
                     }
                  }
               }
               catch (IllegalArgumentException | IllegalAccessException e)
               {
                  throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
               }
            }
         }
      }
      if (!Object.class.equals(cl))
      {
         readTarget(father, me, attributes, cl.getSuperclass());
      }
   }

   /**
    * Gets the attribute value.
    *
    * @param className the class name
    * @param value the value
    * @return the attribute value
    * @throws HandlerException the handler exception
    */
   private Object getAttributeValue(String className, String value) throws HandlerException
   {
      Object ret = null;
      try
      {
         switch (className)
         {
            case "int" :
            case "java.lang.Integer" :
               ret = Integer.parseInt(value);
               break;
            case "float" :
            case "java.lang.Float" :
               ret = Float.parseFloat(value);
               break;
            case "long" :
            case "java.lang.Long" :
               ret = Long.parseLong(value);
               break;
            case "double" :
            case "java.lang.Double" :
               ret = Double.parseDouble(value);
               break;
            case "byte" :
            case "java.lang.Byte" :
               ret = Byte.parseByte(value);
               break;
            case "short" :
            case "java.lang.Short" :
               ret = Short.parseShort(value);
               break;
            case "boolean" :
            case "java.lang.Boolean" :
               ret = Boolean.parseBoolean(value);
               break;
            default :
               ret = value;
         }
      }
      catch (IllegalArgumentException e)
      {
         throw new HandlerException("ObjectHandler", HandlingMethod.READING, e);
      }
      return ret;
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
   
   /**
    * The Class Pair.
    *
    * @param <K> the key type
    * @param <V> the value type
    */
   private static class Pair<K, V>
   {
      
      /** The key. */
      private K key;
      
      /** The value. */
      private V value;
      
      /**
       * Instantiates a new pair.
       *
       * @param key the key
       * @param value the value
       */
      public Pair(K key, V value)
      {
         super();
         this.key = key;
         this.value = value;
      }
      
      /**
       * Gets the key.
       *
       * @return the key
       */
      public K getKey()
      {
         return key;
      }
      
      /**
       * Gets the value.
       *
       * @return the value
       */
      public V getValue()
      {
         return value;
      }
   }
}
