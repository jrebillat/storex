package net.alantea.storex.handlers;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import net.alantea.liteprops.BooleanProperty;
import net.alantea.liteprops.DoubleProperty;
import net.alantea.liteprops.FloatProperty;
import net.alantea.liteprops.IntegerProperty;
import net.alantea.liteprops.LongProperty;
import net.alantea.liteprops.Property;

public final class PrimitiveHandler extends Handler
{
   @SuppressWarnings("serial")
   private static Map<String, Boolean> attributesMap = new HashMap<String, Boolean>() {{
      put(Names.FIELDNAME, false);
      put(Names.CONTENTTAG, true);
      put(Names.ALLOWOTHERTAGS, false);
      put(Names.MAYHAVECHILDREN, false);
      put("property", true);
  }};

   public PrimitiveHandler(Object father, Field field, String type, Map<String, String> attributes) throws HandlerException
   {
      super(father, field, type, attributes);
      String content = attributes.get(Names.CONTENTTAG);
      
      String useProp = attributes.get("property");
      boolean useProperty = (useProp != null) && (useProp.equals("true"));
      
      Object value = null;
      switch(type)
      {
         case Names.FLOATTAG :
            value = Float.parseFloat(content);
            break;

         case Names.INTEGERTAG :
            value = Integer.parseInt(content);
            break;

         case Names.LONGTAG :
            value = Long.parseLong(content);
            break;

         case Names.DOUBLETAG :
            value = Double.parseDouble(content);
            break;

         case Names.BOOLEANTAG :
            value = Boolean.parseBoolean(content);
            break;

         case Names.SHORTTAG :
            value = Short.parseShort(content);
            break;

         case Names.BYTETAG :
            value = Byte.parseByte(content);
            break;
          
      }
      
      if ((useProperty) || ((field != null) && (Property.class.isAssignableFrom(field.getType()))))
      {
         setPropertyTarget(value, father, field);
      }
      else
      {
         setTarget(value, father, field);
      }
   }

   public PrimitiveHandler(Object target)
   {
      super(target);
   }

   @Override
   public void createElement(Object target, String name) throws HandlerException
   {
      String tag = null;
      if ((target instanceof Integer) 
            || ((Integer.TYPE.isAssignableFrom(target.getClass())))
            || (target instanceof IntegerProperty))
      {
         tag = Names.INTEGERTAG;
      }
      else if ((target instanceof Long)
            || ((Long.TYPE.isAssignableFrom(target.getClass())))
            || (target instanceof LongProperty))
      {
         tag = Names.LONGTAG;
      }
      else if ((target instanceof Float)
            || ((Float.TYPE.isAssignableFrom(target.getClass())))
            || (target instanceof FloatProperty))
      {
         tag = Names.FLOATTAG;
      }
      else if ((target instanceof Double)
            || ((Double.TYPE.isAssignableFrom(target.getClass())))
            || (target instanceof DoubleProperty))
      {
         tag = Names.DOUBLETAG;
      }
      else if ((target instanceof Short) || ((Short.TYPE.isAssignableFrom(target.getClass()))))
      {
         tag = Names.SHORTTAG;
      }
      else if ((target instanceof Byte)|| ((Byte.TYPE.isAssignableFrom(target.getClass()))))
      {
         tag = Names.BYTETAG;
      }
      else if ((target instanceof Boolean)
            || ((Boolean.TYPE.isAssignableFrom(target.getClass())))
            || (target instanceof BooleanProperty))
      {
         tag = Names.BOOLEANTAG;
      }

      if (tag != null)
      {
         if (Property.class.isAssignableFrom(target.getClass()))
         {
            write(tag, name, Names.CONTENTTAG, ((Property<?>) target).get().toString(), "property", "true");
         }
         else
         {
            write(tag, name, Names.CONTENTTAG, (String) target.toString(), "property", "false");
         }
      }
   }

   static boolean handlesWrite(Object target) throws HandlerException
   {
      return (target.getClass().isPrimitive())
            || (target instanceof Integer)
            || (target instanceof Double)
            || (target instanceof Float)
            || (target instanceof Long)
            || (target instanceof Byte)
            || (target instanceof Short)
            || (target instanceof Boolean)
            || (target instanceof IntegerProperty)
            || (target instanceof LongProperty)
            || (target instanceof FloatProperty)
            || (target instanceof DoubleProperty)
            || (target instanceof BooleanProperty)
             ;
   }

   static boolean handlesRead(String type) throws HandlerException
   {
      return Names.DOUBLETAG.equals(type)
            || Names.INTEGERTAG.equals(type)
            || Names.FLOATTAG.equals(type)
            || Names.LONGTAG.equals(type)
            || Names.BOOLEANTAG.equals(type)
            || Names.SHORTTAG.equals(type)
            || Names.BYTETAG.equals(type);
   }

   /**
    * Get needed attributes.
    *
    * @return a map of attributes. True if mandatory.
    * @throws HandlerException the handler exception
    */
   static Map<String, Boolean> getHandledAttributes()
   {
      return attributesMap;
   }
}
