Object root# Storex
Storex is a simple storage facility for Java objects in XML format. It is built to store (with some restrictions) simple objects or complex nested objects hierarchies, lists or even maps of objects.

# Overview
Storex is set to store a POJO (or a list or map of POJOs). It should work out of the box for relatively simple hierarchies.

# Using Storex
Using Storex may seem easy,as the methods listed just below are straightforwards, but read carefully the notes below before trying to use it !

## Exception management
All functions may raise a SAXException encapsulating the real cause exception if something has gone wrong.

## Storing data
To store data, Storex offers the following methods :
- `public static void Writer.write(String path, Object root) throws SAXException` will create a file (or overwrite it if it exists) at `path` containing the XML version of the given root object and all its hierarchy underneath.
- `public static void Writer.write(String path, Object root, Map<String, String> map) throws SAXException` will create a file (or overwrite it if it exists) at `path` containing the XML version of the given root object and all its hierarchy underneath. The map will contain placeholder names for some class names, as class names are long string that may be replaced by shorter ones with these placeholders.

## Reading data
To read data, Storex offers the following methods :
- `public static Object Reader.load(String path) throws SAXException` will load the content of a file from `path` and return the recreated root object (or list or map) and all its hierarchy underneath.
- `public static Object Reader.load(java.io.File file) throws SAXException` will load the content of the given file and return the recreated root object (or list or map) and all its hierarchy underneath.
- `public static Object Reader.load(java.io.Reader reader) throws SAXException` will load the content of the given reader and return the recreated root object (or list or map) and all its hierarchy underneath.
- `public static Object Reader.load(org.xml.sax.InputSource source) throws SAXException` will load the content of the given SAX XML source and return the recreated root object (or list or map) and all its hierarchy underneath.

# Objects design
To use all the features from Storex, the calling code must take care of the constraints, limitations and design specifications of the Reader and the Writer.

## Constructors
To be able to recreate the objects, Storex must be able to call some constructor for every object in the hierarchy. To fully understand the following notes, you must understand that objects are nested and each of them will be called in a context (with a father object above it - except for the root one) and with a set of attributes (name/value pairs). Storex is able to manage itself most of the cases. The reader will search for
1. a constructor with two parameters : the father type in the hierarchy and a map of attributes (String, String)
2. a constructor with two parameters : the father type in the hierarchy and a XML Attributes object
3. a constructor with one parameter : the father type in the hierarchy
4. a constructor with one parameter : a XML Attributes object or a map of attributes (String, String)
5. a constructor with no parameter

The result if several matching constructors are available is to use the first one found in the order listed just above. If two constructors meets the 4th point, the result is unpredictable.

## Simple fields
Storex handled without any added information all simple fields (integer, float, long, double, short, byte, boolean) and strings.

## Enums
Storex handles enum fields.

## Lists
Storex handles list fields of simple values or strings just as list of objects without any problem.

## Maps
Storex does handle maps with any key type, any value type.

## Arrays
Storex handles arrays of objects.

## Transient fields
Transient fields are ignored by the Writer.

## Static fields
Static fields are ignored by the Writer.

## Private, protected or public fields
It does not matter : they are handled the same way.

## Derived classes
The fields of the base classes are handled the same way as if they were in the derived classes.

## Properties
Storex manages simple properties using liteprops (integer, float, long, double, boolean and string). It manages also ObjectProperty<?> fields and ListProperty<?> fields. It is unable to manage Javafx properties.

## Special objects
Storex handles Dates and offers a way to add user defined handlers.

# Adding handlers
User defined handlers may be added.

## Handling objects
To handle objects, Storex use a library of known handlers and search in it for the best handler for the object. The handler must be able to handle writing and reading of objects (but not necessarily both equally). Handlers must derive from the Storex Handler class.

# Testing for handling
A handle class must define two static methods :
- `static boolean handlesWrite(Object target) throws HandlerException` : returns true if this handler knows how to write the given object.
- `static boolean handlesRead(Object father, Field field, String type, Attributes attributes)` throws HandlerException : returns true if this handler knows how to create an object from the given information (mostly using the type parameter).

# Handle constructors

## for writing
A writing handler must have a constructor like `public MyHandler(Object target) {super(target);}` that do not need to do more, but must exist.

## for reading
Reading is done directly in the constructor (see below for more). The constructor must be `MyHandler(Object father, Field field, String type, Attributes attributes) throws HandlerException`.

# Writing an object
The method for writing an object is : ` @Override public void createElement(XMLStreamWriter writer, Object target, String name) throws HandlerException`.
The parameters are the SAX XML writer, the target object to writer and the field name where it will be stored in its father (or null if no father).
There are some helper methods for storage? They will be described more in detail in another page :
-  `protected void write(XMLStreamWriter writer, String tagName, String fieldName, String... args) throws HandlerException`
-  `protected void write(XMLStreamWriter writer, String tagName, String fieldName, Map<String, String> attributesMap, String characters) throws HandlerException`
-  `protected void write(XMLStreamWriter writer, String tagName, String fieldName, Object target, Map<String, String> attributesMap, List<Field> fieldsList, String characters) throws HandlerException`
-  `protected void startWriting(XMLStreamWriter writer, String tagName, String fieldName, Map<String, String> attributesMap, String characters) throws HandlerException`
-  `protected final void writeField(XMLStreamWriter writer, Object target, Field field) throws HandlerException`
-  `protected final void endWriting(XMLStreamWriter writer) throws HandlerException`
-  `protected Map<String, String> createMap(String... args)`

# Reading an object
The way reading is done in the dedicated constructor is :
- First the method creates a target object in any way it likes, using the given parameters (the type and the SAX attributes)
- Then it calls `setTarget(target, father, field);` to really insert the object in its father (or not if father is null - but the method should be called anyway).

An optional method may be defined : `public void onEnd(String characters) throws HandlerException` that will be called when reaching the end of the object when reading the file, given the collected characters got during the read. the important point here is that all children have been created when this is called.