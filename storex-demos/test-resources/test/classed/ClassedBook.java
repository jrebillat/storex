package net.alantea.storex.test.classed;

import org.xml.sax.Attributes;

import net.alantea.storex.EndElement;

public class ClassedBook
{
   public ClassedBook(Attributes attributes)
   {
      System.out.println("This is a book");
   }


   @EndElement("classedChapter")
   void onEndScene(ClassedChapter chapter)
   {
      System.out.println("Chapter " + chapter.getNum() + " ended");
   }
}
