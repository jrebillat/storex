package net.alantea.storex.test.classed;

import net.alantea.storex.Writer;
import net.alantea.storex.WriterElement;

public class TestWriterMain
{

   public TestWriterMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args)
   {
      Writer writer = new Writer("test-resources/xmltest1.xml", "classedBook");
      for (int i = 1; i < 5; i++)
      {
         WriterElement elt = writer.getRoot().createChild("classedChapter");
         elt.setAttribute("name", "" + i);
         elt.setAttribute("index", "" + i);
         for (int j = 1; j < 4; j++)
         {
            WriterElement child = elt.createChild("classedScene");
            child.setAttribute("num", "" + j);
            child.setAttribute("name", "Scene index "+ i + "-" + j);
            child.setAttribute("indice", "" + Math.log(i + j));
         }
      }
      writer.finalize();
   }

}
