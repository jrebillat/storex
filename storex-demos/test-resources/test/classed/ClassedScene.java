package net.alantea.storex.test.classed;

import net.alantea.storex.EndElement;

public class ClassedScene
{

   @EndElement("classedScene")
   void onEndScene(ClassedScene me, String code)
   {
      System.out.println("Scene ended (code is " + code + ")");
   }
}
