package net.alantea.storex.test.classed;

import org.xml.sax.Attributes;

import net.alantea.storex.AddElement;

public class ClassedChapter
{
   int num;
   public ClassedChapter(ClassedBook book, Attributes attributes)
   {
      System.out.println("this is chapter " + attributes.getValue("name"));
      num = Integer.parseInt(attributes.getValue("name"));
   }
   
   @AddElement("classedScene")
   public ClassedScene createScene(ClassedScene scene, String name, Attributes attributes)
   {
      System.out.println("creating scene " + attributes.getValue("num"));
      return (scene == null) ? new ClassedScene() : scene;
   }

   public int getNum()
   {
      return num;
   }
}
