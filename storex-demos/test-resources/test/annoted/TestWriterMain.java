package net.alantea.storex.test.annoted;

import net.alantea.storex.Writer;
import net.alantea.storex.WriterElement;

public class TestWriterMain
{

   public TestWriterMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args)
   {
      Writer writer = new Writer("test-resources/xmltest.xml", "root");
      for (int i = 0; i < 5; i++)
      {
         WriterElement elt = writer.getRoot().createChild("Choice");
         elt.setAttribute("name", "" + i);
         elt.setAttribute("aaa", "" + Math.log(i));
         for (int j = 0; j < 4; j++)
         {
            WriterElement child = elt.createChild("Internal-" + i + "-" + j);
            child.setAttribute("aaa", "" + Math.log(i + j));
            child.setAttribute("bbb", "" + Math.log(i + j));
            child.setAttribute("ccc", "" + Math.log(i + j));
         }
      }
      writer.finalize();
   }

}
