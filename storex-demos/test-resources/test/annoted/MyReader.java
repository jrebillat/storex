package net.alantea.storex.test.annoted;

import java.io.File;

import org.xml.sax.Attributes;

import net.alantea.storex.AddElement;
import net.alantea.storex.AnnotedReader;

public class MyReader extends AnnotedReader
{
   public MyReader(File file)
   {
      super(file);
   }
   
   @AddElement("root")
   private void manageRootElement(String name, Attributes attributes)
   {
      System.out.println(name);
   }
   
   @AddElement("Choice")
   private void manageChoice1Element(String name, Attributes attributes)
   {
      System.out.println(name + " " + attributes.getValue("name"));
   }
}
