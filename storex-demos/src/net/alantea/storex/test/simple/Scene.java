package net.alantea.storex.test.simple;

import net.alantea.storex.EndElement;
import net.alantea.storex.Key;
import net.alantea.storex.Store;

@Store("scene")
public class Scene
{
   @Key("index")
   private int num;
   
   private double indice;
   
   /** The chapter name. */
   @Key("name")
   private String name;

   @EndElement("scene")
   void onEndScene(Scene me, String code)
   {
      System.out.println("Scene ended (code is " + num + ")");
   }

   /**
    * Gets the num.
    *
    * @return the num
    */
   public int getNum()
   {
      return num;
   }

   /**
    * Gets the indice.
    *
    * @return the indice
    */
   public double getIndice()
   {
      return indice;
   }

   /**
    * Gets the name.
    *
    * @return the name
    */
   public String getName()
   {
      return name;
   }

   public void setNum(int num)
   {
      this.num = num;
   }

   public void setIndice(double indice)
   {
      this.indice = indice;
   }

   public void setName(String name)
   {
      this.name = name;
   }
}
