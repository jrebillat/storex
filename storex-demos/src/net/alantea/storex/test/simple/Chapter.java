package net.alantea.storex.test.simple;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

import net.alantea.storex.AddElement;
import net.alantea.storex.Child;
import net.alantea.storex.Key;
import net.alantea.storex.Store;

/**
 * The Class ClassedChapter.
 */
@Store("chapter")
public class Chapter
{
   
   /** The chapter number. */
   @Key("index")
   private int num;
   
   /** The chapter name. */
   @Key("name")
   private String name;
   
   @Child("scene")
   private List<Scene> scenes = new ArrayList<>();
   
   /**
    * Instantiates a new classed chapter.
    *
    * @param book the book
    * @param attributes the attributes
    */
   public Chapter(Book book, Attributes attributes)
   {
      System.out.println("this is chapter " + attributes.getValue("name"));
   }
   
   public Chapter()
   {
      // TODO Auto-generated constructor stub
   }

   /**
    * Creates the scene.
    *
    * @param scene the scene
    * @param name the name
    * @param attributes the attributes
    * @return the classed scene
    */
   @AddElement("scene")
   public Scene createScene(Scene scene, String name, Attributes attributes)
   {
      System.out.println("creating scene " + attributes.getValue("index"));
      return (scene == null) ? new Scene() : scene;
   }

   /**
    * Gets the name.
    *
    * @return the name
    */
   public String getName()
   {
      return name;
   }

   /**
    * Gets the num.
    *
    * @return the num
    */
   public int getNum()
   {
      return num;
   }

   public List<Scene> getScenes()
   {
      return scenes;
   }

   public void setNum(int num)
   {
      this.num = num;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public void setScenes(List<Scene> scenes)
   {
      this.scenes = scenes;
   }
}
