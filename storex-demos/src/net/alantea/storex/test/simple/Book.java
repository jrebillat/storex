package net.alantea.storex.test.simple;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

import net.alantea.storex.Child;
import net.alantea.storex.EndElement;
import net.alantea.storex.Store;

@Store("book")
public class Book
{
   
   @Child("chapter")
   private List<Chapter> chapters = new ArrayList<>();


   public Book(Attributes attributes)
   {
      System.out.println("This is a book");
   }

   public Book()
   {
      // TODO Auto-generated constructor stub
   }

   @EndElement("chapter")
   void onEndChapter(Chapter chapter)
   {
      System.out.println("Chapter " + chapter.getNum() + " ended");
   }
   
   public List<Chapter> getChapters()
   {
      return chapters;
   }

   public void setChapters(List<Chapter> chapters)
   {
      this.chapters = chapters;
   }
}
