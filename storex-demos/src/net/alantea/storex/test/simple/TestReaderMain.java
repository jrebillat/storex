package net.alantea.storex.test.simple;

import java.io.File;

import org.xml.sax.SAXException;

import net.alantea.storex.Reader;

public class TestReaderMain
{

   public TestReaderMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args) throws SAXException
   {
      Book book = (Book) Reader.load(new File("test-resources/xmltest2.xml"));
      
      System.out.println("Book");
      for (Chapter chapter : book.getChapters())
      {
         System.out.println("   Chapter " + chapter.getNum() + " - " + chapter.getName());
         for (Scene scene : chapter.getScenes())
         {
            System.out.println("   Scene " + scene.getNum() + " - " + scene.getName() + " (" + scene.getIndice() + ")");
         }
      }
   }
}