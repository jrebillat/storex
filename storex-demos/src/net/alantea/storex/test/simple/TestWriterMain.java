package net.alantea.storex.test.simple;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import net.alantea.storex.Writer;

public class TestWriterMain
{

   public TestWriterMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args) throws SAXException
   {
      Book book = new Book();
      List<Chapter> chapters = new ArrayList<>();
      for (int i = 1; i < 5; i++)
      {
         Chapter chapter = new Chapter();
         chapter.setName("chapter " + i);
         chapter.setNum(i);
         chapters.add(chapter);

         List<Scene> scenes = new ArrayList<>();
         for (int j = 1; j < 4; j++)
         {
            Scene scene = new Scene();
            scene.setName("Scene index "+ i + "-" + j);
            scene.setNum(j);
            scene.setIndice(Math.log(i + j));
            scenes.add(scene);
         }
         chapter.setScenes(scenes);
      }
      book.setChapters(chapters);
      
      Writer.write("test-resources/xmltest3.xml", book);
   }
}
