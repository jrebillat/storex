package net.alantea.storex.test.map;

import net.alantea.storex.Key;
import net.alantea.storex.Store;

@Store("something")
public class MapElement
{
   @Key("index")
   private int num;
   
   private double indice;
   
   public MapElement()
   {
      
   }

   /**
    * Gets the num.
    *
    * @return the num
    */
   public int getNum()
   {
      return num;
   }

   /**
    * Gets the indice.
    *
    * @return the indice
    */
   public double getIndice()
   {
      return indice;
   }

   public void setNum(int num)
   {
      this.num = num;
   }

   public void setIndice(double indice)
   {
      this.indice = indice;
   }
}
