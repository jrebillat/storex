package net.alantea.storex.test.map;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.SAXException;

import net.alantea.storex.Writer;

public class MapWriterMain
{

   public MapWriterMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args) throws SAXException
   {
      Map<String, Object> elements = new HashMap<>();
      for (int i = 1; i < 12; i++)
      {
         MapElement element = new MapElement();
         element.setNum(i);
         element.setIndice(Math.log(i));
         elements.put("chapter " + i, element);
      }
      
      Writer.write("test-resources/xmltest1.xml", elements);
   }
}
