package net.alantea.storex.test.map;

import java.io.File;
import java.util.Map;

import org.xml.sax.SAXException;

import net.alantea.storex.Reader;

public class MapReaderMain
{

   public MapReaderMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args) throws SAXException
   {
      @SuppressWarnings("unchecked")
      Map<String, Object> objects = (Map<String, Object>) Reader.load(new File("test-resources/xmltest1.xml"));
      
      System.out.println("Objects");
      for (String key : objects.keySet())
      {
         MapElement element = (MapElement) objects.get(key);
         System.out.println("   Element " + element.getNum() + " - " + key + " (" + element.getIndice() + ")");
      }
   }
}