package net.alantea.storex.test.list;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import net.alantea.storex.Writer;

public class ListWriterMain
{

   public ListWriterMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args) throws SAXException
   {
      List<Object> elements = new ArrayList<>();
      for (int i = 1; i < 12; i++)
      {
         ListElement element = new ListElement();
         element.setName("chapter " + i);
         element.setNum(i);
         element.setIndice(Math.log(i));
         elements.add(element);
      }
      
      Writer.write("test-resources/xmltest.xml", elements);
   }
}
