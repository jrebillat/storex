package net.alantea.storex.test.list;

import net.alantea.storex.Key;
import net.alantea.storex.Store;

@Store("something")
public class ListElement
{
   @Key("index")
   private int num;
   
   private double indice;
   
   /** The name. */
   @Key("name")
   private String name;
   
   public ListElement()
   {
      
   }

   /**
    * Gets the num.
    *
    * @return the num
    */
   public int getNum()
   {
      return num;
   }

   /**
    * Gets the indice.
    *
    * @return the indice
    */
   public double getIndice()
   {
      return indice;
   }

   /**
    * Gets the name.
    *
    * @return the name
    */
   public String getName()
   {
      return name;
   }

   public void setNum(int num)
   {
      this.num = num;
   }

   public void setIndice(double indice)
   {
      this.indice = indice;
   }

   public void setName(String name)
   {
      this.name = name;
   }
}
