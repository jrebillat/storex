package net.alantea.storex.test.list;

import java.io.File;
import java.util.List;

import org.xml.sax.SAXException;

import net.alantea.storex.Reader;

public class ListReaderMain
{

   public ListReaderMain()
   {
      // TODO Auto-generated constructor stub
   }

   public static void main(String[] args) throws SAXException
   {
      @SuppressWarnings("unchecked")
      List<Object> objects = (List<Object>) Reader.load(new File("test-resources/xmltest.xml"));
      
      System.out.println("Objects");
      for (Object object : objects)
      {
         ListElement element = (ListElement) object;
         System.out.println("   Element " + element.getNum() + " - " + element.getName() + " (" + element.getIndice() + ")");
      }
   }
}